var Tools;

Tools = {
    /**
     * Called only after all JS files are loaded and the DOM ready event was fired.
     */
    init: function(){
        
        this.Tracking.init();
        this.ErrorLog.init(this.Tracking);
        this.Facebook.init(this.Tracking);
        this.Twitter.init(this.Tracking);
        this.GooglePlus.init(this.Tracking);
        this.Scroller.init();
        this.Toggle.init();
        this.Placeholder.init();
        this.Hint.init();
        this.Checkboxes.init();
        this.Selects.init();

    },
    Tracking: {
        init: function(){
            
            var self = this;
            
            $(function(){

                // track all external links
                $("a[href^='http']:not([href*='" + window.location.host + "'])").click(function(){
                    self.track(['External Links'], $(this).attr('href'));
                });

            });
            
        },
        track: function(category, event){
            
            // track in all services
            if (this.TagManager.isAvailable()){
                this.TagManager.track(category, event);
            }else{
                this.Analytics.track(category, event);
            }
            
        },
        Analytics: {
            isAvailable: function(){

                return typeof gtag === 'function';

            },
            track: function(category, name){

                if (this.isAvailable()){
                    var parsed = this.parseCategory(category);
                    gtag('event', name, {
                        'event_category': parsed.category,
                        'event_label': parsed.label
                    });
                }

            },
            parseCategory: function(category){

                if (typeof category === 'string'){
                    return {category: category, label: null};
                }else if (category.length > 2){
                    return {category: category, label: null};
                }else{
                    category = category.slice(0); // clone
                    return {category: category.shift(), label: category.shift()};
                }

            }
        },
        TagManager: {
            isAvailable: function(){

                return typeof dataLayer !== 'undefined' && typeof dataLayer.push === 'function';

            },
            track: function(category, name){
                
                if (this.isAvailable()){
                    dataLayer.push({
                        event: 'track',
                        trackCategory: category,
                        trackAction: name
                   });
                }
                
            }
        }
    },
    /**
     * Track JS errors in analytics.
     */
    ErrorLog: {
        tracker: null,
        init: function(tracker){

            var self = this;

            this.tracker = tracker;

            // catch global errors
            window.onerror = function(message, file, line){
                self.tracker.track(['JavaScript Errors', navigator.userAgent], message + ' (' + file + ':' + line + ')');
                return false;
            };

        }
    },
    /**
     * Twitter related tools.
     */
    Twitter: {
        tracker: null,
        init: function(tracker){

            // save analytics object
            this.tracker = tracker;

            // init child objects
            this.Share.init(this);

            // create shortcuts for most useful methods
            this.share = this.Share.open;

        },
        Share: {
            twitter: null,
            elements: '.twitter-share',
            init: function(twitter){

                var self = this;

                this.twitter = twitter;

                // open the send dialog for the given elements
                $(document).on("click", this.elements, function (event) {

                    event.preventDefault();

                    var $element = $(this);

                    var properties = {}
                    
                    if ($element.attr('data-text'))
                        properties.text = $element.attr('data-text');
                    if ($element.attr('data-url'))
                        properties.url = $element.attr('data-url');
                    if ($element.attr('data-hashtags'))
                        properties.hashtags = $element.attr('data-hashtags');
                    if ($element.attr('data-via'))
                        properties.via = $element.attr('data-via');
                    if ($element.attr('data-related'))
                        properties.related = $element.attr('data-related');

                    // open
                    self.open.call(self, properties);

                });

            },
            open: function(properties){

                // construct the share dialog URL
                var sharer = 'http://twitter.com/share?' + $.param(properties);

                // configure the new browser window
                var x = screen.width / 2 - 550 / 2;
                var y = screen.height / 2 - 450 / 2;
                var settings = 'height=450, width=550, toolbar=0, location=0, menubar=0, directories=0, scrollbars=0, left=' + x + ',top=' + y;

                // track dialog openings
                this.twitter.tracker.track(['Twitter', 'Share'], 'open');

                // open the sharer
                window.open(sharer, 'twitter' + new Date().getTime(), settings);

            }
        }
    },
    /**
     * Google plus related tools.
     */
    GooglePlus: {
        tracker: null,
        init: function(tracker){

            // save analytics object
            this.tracker = tracker;

            // init child objects
            this.Share.init(this);

            // create shortcuts for most useful methods
            this.share = this.Share.open;

        },
        Share: {
            googlePlus: null,
            elements: '.google-plus-share',
            init: function(googlePlus){

                var self = this;

                this.googlePlus = googlePlus;

                // open the send dialog for the given elements
                $(document).on("click", this.elements, function (event) {

                    event.preventDefault();

                    var $element = $(this);

                    var properties = {
                        url: $element.attr('data-url')
                    };

                    // open
                    self.open.call(self, properties);

                });

            },
            open: function(properties){

                // construct the share dialog URL
                var sharer = 'https://plus.google.com/share?' + $.param(properties);

                // configure the new browser window
                var x = screen.width / 2 - 550 / 2;
                var y = screen.height / 2 - 450 / 2;
                var settings = 'height=450, width=550, toolbar=0, location=0, menubar=0, directories=0, scrollbars=0, left=' + x + ',top=' + y;

                // track dialog openings
                this.googlePlus.tracker.track(['Google+', 'Share'], 'open');

                // open the sharer
                window.open(sharer, 'google-plus' + new Date().getTime(), settings);

            }
        }
    },
    /**
     * Facebook related tools.
     */
    Facebook: {
        tracker: null,
        init: function(tracker){

            var self = this;

            // save analytics object
            this.tracker = tracker;

            // init child objects
            this.Login.init(this);
            this.Share.init(this);
            this.Requests.init(this);
            this.Send.init(this);

            // create shortcuts for most useful methods
            this.share = function(){ return self.Share.open.apply(self.Share, arguments); };
            this.requests = function(){ return self.Requests.open.apply(self.Requests, arguments); };
            this.send = function(){ return self.Send.open.apply(self.Send, arguments); };

        },
        Send: {
            facebook: null,
            elements: '.facebook-send',
            init: function(facebook){

                var self = this;

                this.facebook = facebook;

                // open the send dialog for the given elements
                $(document).on("click", this.elements, function (event) {

                    event.preventDefault();

                    var $element = $(this);

                    var properties = {
                        name: $element.attr('data-name'),
                        description: $element.attr('data-description'),
                        link: $element.attr('data-link'),
                        picture: $element.attr('data-picture'),
                        to: $element.attr('data-to')
                    };

                    // open
                    self.open.call(self, properties);

                });

            },
            open: function(properties){

                var self = this;

                properties = $.extend({method: 'send'}, properties);

                this.facebook.tracker.track(['Facebook', 'Send'], 'open');

                FB.ui(properties, function(response) {
                    if (response && response.post_id) {
                        self.facebook.tracker.track(['Facebook', 'Send'], 'success');
                    }
                });

            }
        },
        Requests: {
            facebook: null,
            elements: '.facebook-requests',
            init: function(facebook){

                var self = this;

                this.facebook = facebook;

                // open the requests dialog for the given elements
                $(document).on("click", this.elements, function (event) {

                    event.preventDefault();

                    var $element = $(this);

                    var properties = {
                        message: $element.attr('data-message')
                    };

                    // open
                    self.open.call(self, properties);

                });

            },
            open: function(properties){

                var self = this;

                properties = $.extend({method: 'apprequests'}, properties);

                // track dialog opening
                this.facebook.tracker.track(['Facebook', 'Requests'], 'open');

                FB.ui(properties, function(response) {
                    // track requests success
                    if (response && response.post_id) {
                        self.facebook.tracker.track(['Facebook', 'Requests'], 'success');
                    }
                });

            }
        },
        Share: {
            facebook: null,
            elements: '.facebook-share',
            init: function(facebook){

                var self = this;

                this.facebook = facebook;

                // open the share dialog for the given elements
                $(document).on("click", this.elements, function (event) {

                    event.preventDefault();

                    var $element = $(this);

                    var properties = {
                        name: $element.attr('data-name'),
                        caption: $element.attr('data-caption'),
                        description: $element.attr('data-description'),
                        link: $element.attr('data-link'),
                        picture: $element.attr('data-picture')
                    };

                    if ($element.attr('data-action-name') && $element.attr('data-action-link')){
                        properties.actions = [{
                            name: $element.attr('data-action-name'),
                            link: $element.attr('data-action-link')
                        }];
                    }

                    // open
                    self.open.call(self, properties);

                });
            },
            open: function(properties){

                var self = this;

                properties = $.extend({method: 'feed'}, properties);

                // track dialog
                this.facebook.tracker.track(['Facebook', 'Share'], 'open');

                FB.ui(properties, function(response) {
                    // track success
                    if (response && response.post_id) {
                        self.facebook.tracker.track(['Facebook', 'Share'], 'success');
                    }
                });

            }
        },
        Login: {
            facebook: null,
            elements: 'a.facebook-login',
            init: function(facebook){

                var self = this;

                this.facebook = facebook;

                // require facebook login before continuing on the given elements
                // works with anchors only
                $(document).on("click", this.elements, function (event) {

                    event.preventDefault();

                    var $element = $(this);

                    var options = {};
                    if ($element.attr('data-scope'))
                        options.scope = $element.attr('data-scope');

                    // require login
                    self.require(function(){
                        // redirect
                        location.href = $element.attr('href');
                    }, options);

                });

            },
            /**
             * Calls the callback only if the user logs in.
             */
            require: function(callback, options){

                var self = this;

                FB.getLoginStatus(function(response){
                    if (response.status == 'connected'){
                        // logged in
                        callback();
                    }else{
                        // track opening
                        self.facebook.tracker.track(['Facebook', 'Login'], 'open');
                        // open dialog
                        FB.login(function(response){
                            if (response.authResponse){
                                // logged in
                                // track result
                                self.facebook.tracker.track(['Facebook', 'Login'], 'success');
                                // call the callback
                                callback();
                            }else{
                                // logged out
                                // track result
                                self.facebook.tracker.track(['Facebook', 'Login'], 'failure');
                            }
                        }, options);
                    }
                });

            }
        }
    },
    /*
     * Scrolls smoothly to anchor links.
     */
    Scroller: {
        speed: 500,
        init: function(){

            var self = this;

            $(document).on('click', 'a[href^="#"]:not(.noscroll)', function(event){

                // get the href attribute
                var href = $(this).attr('href');
                // ignore sharp only links
                if (href == '#')
                    return;

                // extract the target ID
                var id = href.substring(1);

                // prevent default action
                event.preventDefault();

                // scroll to the element
                var result = self.scrollTo(id);

                // add the hash id to the location bar
                self.addHref(id);

            });
            
            // monitor push

        },
        addHref: function(id){
            
            // legacy browsers, simply add the hash to the URL
            if (typeof history.replaceState !== 'function'){
                location.href = '#' + id;
                return;
            }
            
            history.replaceState({}, '', '#' + id);
            
        },
        scrollTo: function(target, speed){

            var offset = null;

            if (typeof target == 'number'){
                // scroll to px
                this.scrollToOffset(target, speed);
            }else{
                if (target instanceof $){
                    var $element = target;
                }else{
                    // scroll to a named anchor od element by id
                    // look for a named anchor
                    var $element = $('a[name=' + target + ']');
                    // if not found, look for an element with the given ID
                    if (!$element.length)
                        $element = $('#' + target);
                }
                // scroll to element
                this.scrollToElement($element, speed);
            }

        },
        scrollToOffset: function(offset, speed, scrollParent){

            // use default speed if not provided
            speed = (typeof speed == 'undefined') ? this.speed : speed;
            // use default scrollParent if not provided
            scrollParent = typeof scrollParent !== 'undefined' ? scrollParent : 'html, body';
            
            // smooth scroll
            $(scrollParent).animate({scrollTop: offset}, speed);
            
        },
        scrollToElement: function(element, speed, scrollParent){

            var $element = $(element);
        
            if (!$element.length){
                return;
            }
            
            var $scrollParent = typeof scrollParent === 'undefined' ? this.getScrollParent($element) : this.getScrollParent($(scrollParent));
    
            // no scroll parent
            if (!$scrollParent.length){
                return;
            }

            // compute the scroll difference
            var parentOffset = $scrollParent.is('body, html') ? $scrollParent.scrollTop() : $scrollParent.offset().top;
            var offset = $element.offset().top;

            // scroll this scroll parent
            this.scrollToOffset($scrollParent.scrollTop() + offset - parentOffset, speed, $scrollParent);

            // look for the next scroll parent
            this.scrollToElement($element, speed, $scrollParent)
            
        },
        getScrollParent: function($element){
            
            // make window, body or html the top of the tree
            if ($element.is('body, html'))
                return $();
            
            // get the parent
            var $parent = $element.parent();
            
            // no scroll parent exists
            if (!$parent.length)
                return $();
            
            // body and html are always considered a scroll parent
            if ($parent.is('body, html')){
                return $('body, html');
            }
            
            // check overflow settings
            var overflow = $parent.css('overflow');
            var overflowY = $parent.css('overflow-y');
            
            if (overflow === 'auto' || overflow === 'scroll'){
                return $parent;
            }else{
                if (overflowY === 'auto' || overflowY === 'scroll'){
                    return $parent;
                }else {
                    return this.getScrollParent($parent);
                }
            }
            
        }
    },
    /**
     * Creates buttons that toggle, expand or collapse page elements.
     * E.g:
     * <!-- Toggles (adds class "expanded" or "collapsed" to the elements) all elements with a class set to .details -->
     * <a href="#" data-toggle=".details" >Toggle details</a>
     * <!-- Expands (adds class "expanded" to the elements) all elements with a class set to #secret .hidden -->
     * <a href="#" data-expand="#secret .hidden" >Show hidden secret</a>
     * <!-- Collapses (adds class "collapsed" to the elements) all elements with a class set to #secret .hidden -->
     * <a href="#" data-collapse="#secret .hidden" >Hide hidden secret</a>
     *
     * To set target as relative to the current element, you can use one of the following selector formats:
     * <a href="#" data-collapse="$this < div" >Hide first parent being a div</a>
     * <a href="#" data-collapse="$this > .child" >Hide all direct children having a class child</a>
     * <a href="#" data-collapse="$this ~ .sibling" >Hide all siblings having a class sibilng</a>
     * <a href="#" data-collapse="$this .descendant" >Hide all descendants having a class descendant</a>
     * <a href="#" data-collapse="$this" >Hide this element</a>
     *
     */
    Toggle: {
        elements: {
            toggle: '[data-toggle]',
            expand: '[data-expand]',
            collapse: '[data-collapse]'
        },
        init: function(){

            var self = this;

            // init all target elements state
            $(this.elements.toggle).each(function(){
                
                var target = self.getTarget($(this), $(this).attr('data-toggle'));
                self.update($(this), 'toggle', target);

            });

            $(this.elements.expand).each(function(){

                var target = self.getTarget($(this), $(this).attr('data-expand'));
                self.update($(this), 'expand', target);

            });

            $(this.elements.collapse).each(function(){

                var target = self.getTarget($(this), $(this).attr('data-collapse'));
                self.update($(this), 'collapse', target);

            });

            // bind all events
            this.bind();


        },
        bind: function(){

            var self = this;

            // bind toggle event
            $(document).on('click change', this.elements.toggle, function(event){
                
                if ($(this).is('a'))
                    event.preventDefault();

                var target = self.getTarget($(this), $(this).attr('data-toggle'));
                self.toggle($(this), target);

            });

            // bind expand event
            $(document).on('click change', this.elements.expand, function(event){

                if ($(this).is('a'))
                    event.preventDefault();

                var target = self.getTarget($(this), $(this).attr('data-expand'));
                self.expand($(this), target);

            });

            // bind collapse event
            $(document).on('click change', this.elements.collapse, function(event){

                if ($(this).is('a'))
                    event.preventDefault();

                var target = self.getTarget($(this), $(this).attr('data-collapse'));
                self.collapse($(this), target);

            });

        },
        update: function($element, action, target){

            var $target = $(target);

            var state = null;

            // follow the state of radio buttons and input boxes
            if (($element.is('input[type=checkbox],input[type=radio]'))){
                switch(action){
                    case 'expand':
                        state = $element.is(':checked') ? 'expanded' : null;
                        break;
                    case 'collapse':
                        state = $element.is(':checked') ? 'collapsed' : null;
                        break;
                    case 'toggle':
                        state = $element.is(':checked') ? 'expanded' : 'collapsed';
                        break;
                }
            }

            // a valid state is already set on the target element
            if(($target.hasClass('expanded') || $target.hasClass('collapsed')) && state === null)
                return;

            // set the current state, use expanded by default
            $(target).removeClass('collapsed expanded').addClass(state !== null ? state : 'expanded');

        },
        expand: function($element, target){
            
            if ($element.is('input[type=checkbox]:not(:checked),input[type=radio]:not(:checked)'))
                return;

            $(target).removeClass('collapsed').addClass('expanded');

        },
        collapse: function($element, target){

            if ($element.is('input[type=checkbox]:not(:checked),input[type=radio]:not(:checked)'))
                return;

            $(target).removeClass('expanded').addClass('collapsed');

        },
        toggle: function($element, target){
            
            var state = null;

            if ($element.is('input[type=checkbox],input[type=radio]'))
                state = $element.is(':checked') ? 'expanded' : 'collapsed';
            
            if (state)
                $(target).removeClass('expanded collapsed').addClass(state);
            else
                $(target).toggleClass('expanded').toggleClass('collapsed');

        },
        getTarget: function($element, selector){

            // look fot $this keyword
            var format = /^([\s]*\$this)?(.*)$/.exec(selector);

            // expand results
            var usesThis = format[1] ? true : false;
            var path = format[2];

            if (usesThis){
                // this selector uses the $this keyword

                // parse the path
                var format = /^[\s]*([\~\>\<])?[\s]*(.*)$/.exec(path);

                // expand results
                var selector = format[1];
                var path = format[2];

                switch(selector){
                    case '~':
                        return $element.siblings(path);
                    case '>':
                        return $element.children(path).first();
                    case '<':
                        return $element.parents(path).first();
                    default:
                        // no selector, look for descendants or take this element
                        if (path)
                            return $element.find(path);
                        else
                            return $element;
                }

            }else{
                // this is a simple selector
                return path;
            }

        }
    },
    /**
     * Custom placeholder for form elements.
     * When the HTML5 placeholder cursor color gets fixed in Firefox, this can be replaced with standard placeholder and a polyfill.
     */
    Placeholder: {
        init: function(){
            
            // initiate on all input and text area elements with a placholder defined
            var inputs = $('input[data-placeholder], textarea[data-placeholder]');
            // add placholders
            this.add(inputs);
            
        },
        add: function(elements, placeholder){

            var self = this;

            // set placeholder value, if provided
            if (typeof placeholder != 'undefined')
                elements.attr('data-placeholder', placeholder);

            // bind handlers
            elements.focus(function(){
                self.focus.call(self, this);
            });
            elements.blur(function(){
                self.blur.call(self, this);
            });

            // init
            elements.each(function(){
                self.blur.call(self, this);
            });

        },
        focus: function(target){

            var $target = $(target);

            if ($target.is('[readonly]') || $target.is('[disabled]'))
                return;

            var placeholder = $target.attr('data-placeholder');
            
            if ($target.val() == placeholder){
                $target.val('');
            }
            
            $target.removeClass('placeholder');

        },
        blur: function(target){

            var $target = $(target);

            if ($target.is('[readonly]') || $target.is('[disabled]'))
                return;

            var placeholder = $target.attr('data-placeholder');
            
            /** prevent for equal value placeholder and value input */
            $target.parents('form').submit(function(){
               
               if($target.val() == placeholder)
                   $target.val('');
                
            });

            if ($target.val() == ''){
                $target.val(placeholder).addClass('placeholder');
            }

        }
    },
    /**
     * Adds a hint to all elements with data-hint property specified.
     */
    Hint: {
        elements: '[data-hint]',
        init: function() {

            var self = this;

            $(document).on('mouseover', this.elements, function(){

                var $element = $(this);

                self.show($element, $element.attr('data-hint'));

            });

            $(document).on('mouseout', this.elements, function(){

                var $element = $(this);

                self.hide($element);

            });

        },
        show: function($element, html){

            $hint = $('<span />').addClass('hint');
            $element.append($hint);

            $hint.html(html);

        },
        hide: function($element){

            $element.find('.hint').remove();

        }
    },
    /**
     * Enables custom checkboxes.
     */
    Checkboxes: {
        elements: 'input[type=checkbox].custom, input[type=radio].custom',
        init: function(){

            var self = this;

            $(this.elements).each(function(){

                // find input element
                var $input = $(this);

                // find all associated labels
                var $label = $input.parents('label');
                if ($input.attr('id'))
                    $label = $label.add('label[for=' + $input.attr('id') + ']');

                // create a new container with the same classes
                var $container = $('<span />').attr('class', $input.attr('class')).addClass($input.is('[type=checkbox]') ? 'checkbox' : 'radio');

                if ($input.is(':checked'))
                    $container.addClass('checked');

                // move the original input inside
                // display: none would result of ignoring the input in IE 9 and older, hide the inputs by positioning them far left
                $input.css({
                    position: 'absolute',
                    left: '-2999px'
                }).after($container);
                $container.append($input);

                // bind
                $container.click(function(event){

                    // do nothing if this is a click event on the hidden input element (probably generated as a result of clicking the associated label)
                    if ($(event.target).is('input'))
                        return;

                    // do nothing, if wrapped in a label, this will change the input value automatically
                    if ($(this).parents('label').length)
                        return;

                    self.toggle($(this));
                });

                // bind label mouseover
                $label.mouseover(function(){
                    $container.addClass('label-hover');
                });
                $label.mouseout(function(){
                    $container.removeClass('label-hover');
                });

                // monitor input state
                $input.bind('change click', function(event){

                    self.update($container);

                    // if this is a radio button, update all other radio buttons in this set as well
                    if ($(this).is('input[type=radio]')){
                        $set = $('input[type=radio][name="' + $(this).attr('name') + '"]');
                        $set.parent('.radio').each(function(){
                            self.update($(this));
                        });
                    }

                });

            });


        },
        toggle: function($container){

            var $input = $container.find('input[type=checkbox],input[type=radio]');

            if ($input.is(':checked'))
                $input.get(0).checked = false;
            else
                $input.get(0).checked = true;

            $input.change();

        },
        update: function($container){

            if ($container.find('input[type=checkbox],input[type=radio]').is(':checked'))
                $container.addClass('checked');
            else
                $container.removeClass('checked');

        }
    },
    /**
     * Enables custom selects.
     */
    Selects: {
        elements: 'select.custom',
        init: function(){

            var self = this;

            $(this.elements).each(function(){

                var $select = $(this);

                // create new custom DOM objects
                var $container = self.render($select);

                // bind
                // expand / collapse on click on the container
                $container.click(function(event){
                    // do nothing
                    event.preventDefault();
                    // do not propagate to document
                    // clicks on document collapse the select menu
                    event.stopPropagation();
                    // toggle
                    self.toggle($container);
                });

                // collapse on click outside of the container
                $(document).click(function(){
                    self.collapse($container);
                });

                // choose an option
                $container.on('click', '.option', function(){
                    self.option($container, $(this));
                });

                // monitor input state
                $select.change(function(){
                    self.update($container);
                });

                // init state
                self.update($container);

            });


        },
        render: function($select){

            // create a new container with the same classes
            var $container = $('<div />').attr('class', $select.attr('class')).addClass('select');

            // create the collapsed UI control
            var $control = $('<div />').addClass('control').appendTo($container);
            var $label = $('<span />').addClass('label')
                                      .text($select.find('option:selected').text())
                                      .appendTo($control);
            var $expand = $('<span />').addClass('expand').appendTo($control);

            // create a list of options
            // create the list container
            var $options = $('<ul />').addClass('options').appendTo($container);
            // create list options
            $select.find('option').each(function(){

                // create the element
                var $option = $('<li />').attr('class', $(this).attr('class')).addClass('option');

                // set label
                $option.text($(this).text());
                // set option value
                $option.attr('data-value', $(this).val());

                // add to the list element
                $options.append($option);

            });

            // move the original input inside
            $select.hide().after($container);
            $container.append($select);

            return $container;

        },
        toggle: function($container){

            $container.toggleClass('active');

        },
        collapse: function($container){

            $container.removeClass('active');

        },
        expand: function($container){

            $container.addClass('active');

        },
        option: function($container, $option){

            this.value($container, $option.attr('data-value'));

        },
        value: function($container, value){

            $container.find('select').val(value).change();

        },
        update: function($container){

            var $selected = $container.find('select option:selected');
            var label = $selected.text();
            var value = $selected.val();

            $container.find('.label').text(label);
            $container.find('.option').removeClass('active')
                      .filter('[data-value="' + value + '"]').addClass('active');

        }
    }
};

// Initiate the application
jQuery(function() {
    Tools.init();
});