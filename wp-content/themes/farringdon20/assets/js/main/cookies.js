Cookies = {
    ui: {
        container: '.js-cookies',
        agree: '.js-cookies__agree'
    },
    cookieName: 'cookies-20farringdonstreet-agreed',
    ready: function () {

        if (!$(this.ui.container).length){
            return;
        }

        this.bind();

        // init state
        if (this.hasCookie()){
            this.hide();
        }else{
            this.show();
        }

    },
    bind: function () {

        var self = this;

        // click on the agree button
        $(document).on('click', this.ui.agree, function (event) {
            event.preventDefault();
            self.saveCookie();
            self.hide();
        });

    },
    saveCookie: function () {

        var date = new Date();
        date.setFullYear(date.getFullYear() + 10);
        document.cookie = this.cookieName + '=1; path=/; expires=' + date.toGMTString();

    },
    hasCookie: function () {
        var name = this.cookieName + '=';
        for (var ca = document.cookie.split(/;\s*/), i = ca.length - 1; i >= 0; i--){
            // found
            if (!ca[i].indexOf(name)){
                return true;
            }
        }
        // not found
        return false;
    },
    hide: function () {

        $(this.ui.container).removeClass('active');

    },
    show: function () {

        $(this.ui.container).addClass('active');

    }
};

$(function(){
    //Cookies.ready();
});