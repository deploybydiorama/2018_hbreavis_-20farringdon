var Application;

Application = {
    ready: function () {

        this.Tracking.ready();

    },
    Tracking: {
        ready: function(){

            html_forms.on('success', function(formElement) {
                Tools.Tracking.track('form/' + $(formElement).data('slug'), 'success');
            });

        }
    }
};

// Initiate the application
jQuery(function () {
    Application.ready();
});