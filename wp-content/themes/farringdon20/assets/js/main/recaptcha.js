(function($){
    window.RecaptchaForm = {
        key: null,
        prefillInterval: 90000,
        ui: {
            form: 'form:not(.hf-form):not([data-recaptcha-prefill])',
            prefillForm: 'form.hf-form,form[data-recaptcha-prefill]',
        },
        ready: function () {

            var self = this;

            // load key
            if (!this.key && typeof recaptchaSiteKey != 'undefined'){
                this.key = recaptchaSiteKey;
            }

            // recaptcha ready
            window.recaptchaLoaded = function(){
                // prefill unsupported forms
                if($(self.ui.prefillForm).length){
                    self.autoprefillTokens();
                }
            }

            // load script
            self.initScript();

            // verify supported forms on submit
            $(document).on('submit', this.ui.form, function(e){
                self.onSubmit(this, e);
            });
        },
        onSubmit: function(form, e){

            // check if a new token is ready
            if ($(form).data('g-recaptcha-response-ready')){
                $(form).removeData('g-recaptcha-response-ready'); // reset
                return;
            }

            // wait for a new token
            e.preventDefault();

            self.getRecaptchaToken(form, 'form_submit', function(token){

                // add token to the form
                self.setFormToken(form, token);

                // submit
                $(form).data('g-recaptcha-response-ready', token);
                $(form).submit();

            });

        },
        autoprefillTokens: function (){

            var self = this;

            $(this.ui.prefillForm).each(function(){
                var form = this;
                self.prefillToken(form);
                setInterval(function(){
                    self.prefillToken(form)
                }, self.prefillInterval);
            });

        },
        prefillToken: function(form){

            var self = this;

            this.getRecaptchaToken(form, 'form_prefill', function(token){
                // add token to the form
                self.setFormToken(form, token);
            });

        },
        initScript: function(){

            $('body').append('<script src="https://www.google.com/recaptcha/api.js?render=' + this.key + '&onload=recaptchaLoaded"></script>')

        },
        getFormToken: function(form){

            return $(form).find('input[name="g-recaptcha-response"]').val();

        },
        setFormToken: function(form, token){

            // get or create input
            var $input = $(form).find('input[name="g-recaptcha-response"]');
            if (!$input.length){
                $input = $('<input type="hidden" name="g-recaptcha-response" />');
                $(form).prepend($input);
            }

            // set value
            $input.val(token);

        },
        getRecaptchaToken: function(form, action, callback){

            var self = this;

            if (!this.key){
                return;
            }

            grecaptcha.ready(function() {
                grecaptcha.execute(self.key, {action: action}).then(callback);
            });

        }
    }
})(jQuery);

// Initiate the application
jQuery(function () {
    RecaptchaForm.ready();
});