var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var playerInfoList = [{
    id: 'player1',
    height: '390',
    width: '640',
    videoId: 'b47UbTfXByU'
}, {
    id: 'player2',
    height: '390',
    width: '640',
    videoId: 'HPjJhmjkDoA'
}];

function onYouTubeIframeAPIReady() {
    if (typeof playerInfoList === 'undefined') return;

    for (var i = 0; i < playerInfoList.length; i++) {
        var curplayer = createPlayer(playerInfoList[i]);
        players[i] = curplayer;
    }
}

var players = new Array();

function createPlayer(playerInfo) {
    return new YT.Player(playerInfo.id, {
        height: playerInfo.height,
        width: playerInfo.width,
        videoId: playerInfo.videoId,
    });
}
//-------------------------------------------
//youtube videos
$('.section-videos .video-cover').on('click',function(){
    var $vid = $(this).siblings('.video-placeholder').attr('id');
    $(this).addClass('hidden');
    $ytid = playerInfoList
        .map(function (element) {return element.id;})
        .indexOf($vid);
    $(players)[$ytid].playVideo();
});