//-------------------------------------------
//global variables
window.screenXL = 1300;
window.screenLG = 1100;
window.screenMD = 992;
window.screenSM = 767;
window.screenXS = 480;
window.screenUS = 380;
(function ($) {
	$(document).ready(function(){

        $('.js-popup-trigger-1').click(function(){
            $('.js-popup-1').toggleClass('active');
        });

		//-------------------------------------------
		//true window dimensions
		function viewport() {
			var e = window, a = 'inner';
			if (!('innerWidth' in window )) {
				a = 'client';
				e = document.documentElement || document.body;
			}
			return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
		}
        //-------------------------------------------
        //toggle body class
        function fnToggle(cl,action) {
            var $activeClass = cl,
                $elClass = $('body');
            if(action=='close'){
                $elClass.removeClass($activeClass);
            }else{
                if($elClass.hasClass($activeClass)==true){
                    $elClass.removeClass($activeClass);
                }else{
                    $elClass.addClass($activeClass);
                }
            }
        }
        //-------------------------------------------
        //link
        $(document).on('click','*[data-link]',function(){
            window.location.href = $(this).attr('data-link');
        });
        //-------------------------------------------
        //mobile nav
        function fnToggleNav(action) {
            fnToggle('nav-active',action);
        }
        $(document).on('click','#header .toggle-nav',function(e){
            e.preventDefault();
            fnToggleNav();
        });
        $(document).keyup(function(e) {
            if (e.keyCode === 27){
                fnToggleNav('close');
            }
        });
        //-------------------------------------------
        //sliders
        $('.section-slider .slider').slick({
            arrows: false,
            autoplay: true,
            autoplaySpeed: 5000,
            dots: true,
            draggable: true,
            infinite: true,
            prevArrow: '<a class="slick-dir-nav prev"></a>',
            nextArrow: '<a class="slick-dir-nav next"></a>',
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            swipe: true,
            customPaging: function(slider,i){
                return '<a class="tab"></a>';
            }
        });
        $('.section-top .slider').slick({
            arrows: false,
            dots: true,
            draggable: true,
            infinite: true,
            prevArrow: '<a class="slick-dir-nav prev"></a>',
            nextArrow: '<a class="slick-dir-nav next"></a>',
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            swipe: true,
            customPaging: function(slider,i){
                return '<a class="tab"></a>';
            }
        });
        //-------------------------------------------
        //Floor plans - slick

/*        function customSlick(images, controls, control, controlPrev, controlNext, desName, desArea, popUpName, popUpDes, popUpImg) {

            $(images).slick({
                arrows: true,
                dots: false,
                draggable: false,
                fade: true,
                infinite: true,
                prevArrow: $(controlPrev),
                nextArrow: $(controlNext),
                speed: 500,
                autoplay: false,
                autoplaySpeed: 8000,
                slidesToShow: 1,
                slidesToScroll: 1,
                swipe: false,
                customPaging: function(slider,i){
                    return '<a class="tab"></a>';
                },
                responsive: [{
                    breakpoint: screenSM,
                    settings: {
                        dots: true,
                        draggable: true,
                        fade: false,
                        swipe: true
                    }
                }]
            });

            $(controls).on('click',function(e){
                var $el = $(this),
                    $i = $el.index(),
                    $clActive = 'selected';
                //console.log($i);
                e.preventDefault();
                $(images).slick('slickGoTo',$i);
                if($el.hasClass($clActive)==false){
                    $el.siblings('.'+$clActive).removeClass($clActive);
                    $el.addClass($clActive);
                }
            });

            $(images).on('beforeChange', function(event, slick, currentSlide, nextSlide){
                var tabs = $(control);
                tabs.children().removeClass('selected');
                tabs.children().eq(nextSlide).addClass('selected');

                //console.log(currentSlide);

                var text = tabs.children().eq(nextSlide).find('.t-cell')[0].textContent;
                var areaFeet = tabs.children().eq(nextSlide).find('.t-cell')[1].textContent;
                var areaMeters = tabs.children().eq(nextSlide).find('.t-cell')[2].textContent;

                $(desName).text(text +' FLOOR');
                $(desArea).text(areaFeet + ' SQ FT / ' + areaMeters + ' SQ M')
                $(popUpName).text(text +' FLOOR');
                $(popUpDes).text(areaFeet + ' SQ FT / ' + areaMeters + ' SQ M')
            });

            $(images).on('afterChange', function(event, slick, currentSlide, nextSlide){
                //polute popup picture
                var source = $( popUpImg + '.slick-current').attr('src');
                $(popUpImg + 'img').attr('src', source);
            });

        }

        customSlick('.js-floor-plan__image', '.js_floor_plans-selectable', '.js-floor-tabs', '.js-floor-plan__control--previous',
            '.js-floor-plan__control--next', '.js-floor-plan__name', '.js-floor-plan__description',
            '.js-popup-floor-plan__name', '.js-popup-floor-plan__description', '.js-floor-plan__image');*/




//////////////////////////////////////////////////

        $('.js-floor-plan__image').slick({
            arrows: true,
            accessibility: false,
            dots: false,
            draggable: false,
            fade: true,
            infinite: true,
            prevArrow: $('.js-floor-plan__control--previous'),
            nextArrow: $('.js-floor-plan__control--next'),
            speed: 500,
            autoplay: false,
            autoplaySpeed: 8000,
            slidesToShow: 1,
            slidesToScroll: 1,
            swipe: false,
            customPaging: function(slider,i){
                return '<a class="tab"></a>';
            },
            responsive: [{
                breakpoint: screenSM,
                settings: {
                    dots: true,
                    draggable: true,
                    fade: false,
                    swipe: true
                }
            }]
        });

        $('.js_floor_plans-selectable').on('click',function(e){
            var $el = $(this),
                $i = $el.index(),
                $clActive = 'selected';
            //console.log($i);
            e.preventDefault();
            $('.js-floor-plan__image').slick('slickGoTo',$i);
            if($el.hasClass($clActive)==false){
                $el.siblings('.'+$clActive).removeClass($clActive);
                $el.addClass($clActive);
            }
        });

        $('.js-floor-plan__image').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            var tabs = $('.js-floor-tabs');
            tabs.children().removeClass('selected');
            tabs.children().eq(nextSlide).addClass('selected');

            //console.log(currentSlide);

            var text = tabs.children().eq(nextSlide).find('.t-cell')[0].textContent;
            var areaFeet = tabs.children().eq(nextSlide).find('.t-cell')[1].textContent;
            var areaMeters = tabs.children().eq(nextSlide).find('.t-cell')[2].textContent;

            $('.js-floor-plan__name').text(text +' FLOOR');
            $('.js-floor-plan__description').text(areaFeet + ' SQ FT / ' + areaMeters + ' SQ M')
            $('.js-popup-floor-plan__name').text(text +' FLOOR');
            $('.js-popup-floor-plan__description').text(areaFeet + ' SQ FT / ' + areaMeters + ' SQ M')
        });

        $('.js-floor-plan__image').on('afterChange', function(event, slick, currentSlide, nextSlide){
            //polute popup picture
            var source = $('.js-floor-plan__image .slick-current').attr('src');
            $('.js-popup-floor-plan__image img').attr('src', source);
        });



        //-------------------------------------------
        //Space plans - slick


        $('.js_space_plans').slick({
            arrows: true,
            accessibility: false,
            dots: false,
            draggable: false,
            fade: true,
            infinite: true,
            prevArrow: $('.js_space_plans--previous'),
            nextArrow: $('.js_space_plans--next'),
            speed: 500,
            autoplay: false,
            autoplaySpeed: 8000,
            slidesToShow: 1,
            slidesToScroll: 1,
            swipe: false,
            customPaging: function(slider,i){
                return '<a class="tab"></a>';
            },
            responsive: [{
                breakpoint: screenSM,
                settings: {
                    dots: true,
                    draggable: true,
                    fade: false,
                    swipe: true
                }
            }]
        });

        $('.js_space_plans-selectable').on('click',function(e){
            var $el = $(this),
                $i = $el.index(),
                $clActive = 'selected';
            //console.log($i);
            e.preventDefault();
            $('.js_space_plans').slick('slickGoTo',$i);
            if($el.hasClass($clActive)==false){
                $el.siblings('.'+$clActive).removeClass($clActive);
                $el.addClass($clActive);
            }

            //show spaceplan details
/*            $('.js-spaceplans-details').children().addClass('js-hide');
            $('.js-spaceplans-details').children().eq($i).removeClass('js-hide');*/

        });

        $('.js_space_plans').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            var tabs = $('.js-space-tabs');
            tabs.children().removeClass('selected');
            tabs.children().eq(nextSlide).addClass('selected');


            $('.js-spaceplans-details').children().addClass('js-hide');
            $('.js-spaceplans-details').children().eq(nextSlide).removeClass('js-hide');

            //console.log(currentSlide);

            var text = tabs.children().eq(nextSlide).find('.t-cell')[0].textContent;
            var areaFeet = tabs.children().eq(nextSlide).find('.t-cell')[1].textContent;
            var areaMeters = tabs.children().eq(nextSlide).find('.t-cell')[2].textContent;

            $('.js-space-plan__name').text(text +' FLOOR');
            $('.js-space-plan__description').text(areaFeet + ' SQ FT / ' + areaMeters + ' SQ M')
            $('.js-popup-space-plan__name').text(text +' FLOOR');
            $('.js-popup-space-plan__description').text(areaFeet + ' SQ FT / ' + areaMeters + ' SQ M')



        });

        $('.js_space_plans').on('afterChange', function(event, slick, currentSlide, nextSlide){
            //polute popup picture
            var source = $('.js_space_plans .slick-current').attr('src');
            $('.js-popup-space-plan__image img').attr('src', source);
        });




        //-------------------------------------------
        //Area plans - slick


/*        $('.js_area_plans').slick({
            arrows: true,
            accessibility: false,
            dots: false,
            draggable: false,
            fade: true,
            infinite: true,
            prevArrow: $('.js_area_plans--previous'),
            nextArrow: $('.js_area_plans--next'),
            speed: 500,
            autoplay: false,
            autoplaySpeed: 8000,
            slidesToShow: 1,
            slidesToScroll: 1,
            swipe: false,
            customPaging: function(slider,i){
                return '<a class="tab"></a>';
            },
            responsive: [{
                breakpoint: screenSM,
                settings: {
                    dots: true,
                    draggable: true,
                    fade: true,
                    swipe: false
                }
            }]
        });*/

        $('.js_area_plans-selectable').on('click',function(e){
            var $el = $(this),
                $i = $el.index(),
                $clActive = 'selected';
            //console.log($i);
            e.preventDefault();
            $("#profile-tab").tab('show');
            $('.js-floor-plan__image').slick('slickGoTo',$i);

/*            $('.js_area_plans').slick('slickGoTo',$i);
            if($el.hasClass($clActive)==false){
                $el.siblings('.'+$clActive).removeClass($clActive);
                $el.addClass($clActive);
            }*/
        });

/*        $('.js_area_plans').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            var tabs = $('.js-area-tabs');
            tabs.children().removeClass('selected');
            tabs.children().eq(nextSlide).addClass('selected');

            //console.log(currentSlide);

            var text = tabs.children().eq(nextSlide).find('.t-cell')[0].textContent;
            var areaFeet = tabs.children().eq(nextSlide).find('.t-cell')[1].textContent;
            var areaMeters = tabs.children().eq(nextSlide).find('.t-cell')[2].textContent;

            $('.js-area-plan__name').text(text +' FLOOR');
            $('.js-area-plan__description').text(areaFeet + ' SQ FT / ' + areaMeters + ' SQ M')
            $('.js-popup-area-plan__name').text(text +' FLOOR');
            $('.js-popup-area-plan__description').text(areaFeet + ' SQ FT / ' + areaMeters + ' SQ M')
        });


        $('.js_area_plans').on('mouseenter', '.js_area_plans-selectable', function (e) {
            var $currTarget = $(e.currentTarget),
                index = $currTarget.data('slick-index'),
                slickObj = $('.js_area_plans-selectable').slick('getSlick');

            slickObj.slickGoTo(index);

        });*/

        //color mapSVG from hovering js-area-tabs
        var tabs = $('.js-area-tabs');
        tabs.children().hover(
            function() {
                var $el = $(this)
                var $i = $el.index();
                var revert = 11 - $i;
                var path = revert > 9 ? '#0' + revert : '#00' + revert;

                $(path)[0].style.fill = 'rgba(193, 131, 100, 0.9)';
            }, function() {
                var $el = $(this)
                var $i = $el.index();
                var revert = 11 - $i;
                var path = revert > 9 ? '#0' + revert : '#00' + revert;
                $(path)[0].style.fill = 'rgba(193, 131, 100, 0.1)';
            }
            );


        //-------------------------------------------
        //refresh slick on tab change


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            //$('.js_area_plans').get(0).slick.setPosition();
            $('.js_space_plans').get(0).slick.setPosition();
            $('.js-floor-plan__image').get(0).slick.setPosition();
        });

        //-------------------------------------------
        //scroll functionality
        function locationMapposition() {
            var $holder = $('.section-location-discover .map-holder');
            if($holder.length){
                var $sect = $('.section-location-discover'),
                    $ph = $('.section-location-discover .map-placeholder'),
                    $left = $ph.offset().left,
                    $sectTop = $sect.offset().top,
                    $phTop = $ph.offset().top,
                    $top = $phTop - $sectTop,
                    $phWidth = $ph.outerWidth(),
                    $phHeight = $ph.outerHeight(),
                    $width = $phWidth + $left;
                $('.section-location-discover .map-holder').css({top: $top,width: $width,height: $phHeight});
            }
        }
        locationMapposition();
        $(window).on('load',function(){
            locationMapposition();
        });
        $(window).resize(function(){
            locationMapposition();
        });


        //-------------------------------------------
        //fixed header
        function fnFixedHeader() {
            var $headerHeight = $('#header').outerHeight();
            $('#header-placeholder').height($headerHeight);
        }
        fnFixedHeader();
        $(window).on('load',function(){
            fnFixedHeader();
        });
        $(window).resize(function(){
            fnFixedHeader();
        });
        //-------------------------------------------
        //scroll functionality
        // $('a[href*="#"]:not([href="#"]):not(.js-popup-inline)').click(function() {
        //     if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname){
        //         var target = $(this.hash),
        //             headerHeight = $('#header').outerHeight();
        //         target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        //         if (target.length) {
        //             $('html,body').animate({
        //                 scrollTop: target.offset().top - headerHeight
        //             }, 500);
        //             return false;
        //         }
        //     }
        // });
        //-------------------------------------------
        //top part scrolled
        function fnDesktopFixedNav(el) {
            var $el = el,
                $cl = 'fixed-nav-active';
            if($el.length){
                if($el.outerHeight() < $(window).scrollTop()){
                    $('body').addClass($cl);
                }else{
                    $('body').removeClass($cl);
                }
            }
        }
        fnDesktopFixedNav($('.section-top'));
        $(window).on('load',function(){
            fnDesktopFixedNav($('.section-top'));
        });
        $(window).resize(function(){
            fnDesktopFixedNav($('.section-top'));
        });
        $(window).scroll(function(){
            fnDesktopFixedNav($('.section-top'));
        });
        //-------------------------------------------
	});

	//nav bar active
    $(document).ready(function ($) {
        var url = window.location.href;
        var activePage = url;
        $('.main-nav a').each(function () {
            var linkPage = this.href;

            if (activePage == linkPage) {
                $(this).addClass("active");
            }
        });
    });


})(jQuery)