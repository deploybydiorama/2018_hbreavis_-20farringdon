(function($){
    window.AmIHuman = {
        ui: {
            form: 'form',
            input: 'input[name="aih"]',
        },
        ready: function () {

            var self = this;

            $(document).on('submit', this.ui.form, function(){
                // fill in the input value before submit
                self.fill(this);
            });

            $(this.ui.form).each(function(){
                // fill in the input value on load
                self.fill(this);
            });
        },
        fill: function(form){

            var $input = $(form).find(this.ui.input);
            $input.val($input.attr('data-aih'));

        }
    }
})(jQuery);

// Initiate the application
jQuery(function () {
    AmIHuman.ready();
});