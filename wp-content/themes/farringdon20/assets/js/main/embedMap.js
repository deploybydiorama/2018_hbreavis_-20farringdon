var EmbedMap = {
    ui: {
        container: '.js-map',
        embed: '.js-map__embed',
        group: '.js-map__group',
        place: '.js-map__place'
    },
    defaultStyles: [
        {
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e6b53"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#f2f2f2"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#e6f3d6"
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "lightness": 45
                },
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#eee0da"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "simplified"
                },
                {
                    "color": "#c18364"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#484848"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#e2c6b9"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#787878"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                },
                {
                    "hue": "#ff0000"
                }
            ]
        },
        {
            "featureType": "transit.station.rail",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#eaf6f8"
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#eaf6f8"
                }
            ]
        }
    ],
    ready: function () {

        $('<script src="https://maps.googleapis.com/maps/api/js?key=' + option.google_maps_key + '&callback=EmbedMap.load" ></script>').appendTo('body');

    },
    load: function () {

        var self = this;

        $(this.ui.container).each(function () {
            self.createMap(this);
        });

    },
    createMap: function(container){

        var markers = [];
        var locationInfowindow = null;

        var self = this;

        // create map
        var $embed = $(container).find(this.ui.embed);
        var map = new google.maps.Map($embed.get(0), {
            center: {lat: $embed.data('lat'), lng: $embed.data('lng')},
            zoom: $embed.data('zoom'),
            styles: this.defaultStyles,
            mapTypeControl: false,
            streetViewControl: false,
            fullscreenControl: false
        });
window.m = map;

        // place the main marker
        var icon = {
            url: link.themePath + '/public/i/marker.png',
            scaledSize: new google.maps.Size(48, 152),
            anchor: new google.maps.Point(24, 152),
        };
        new google.maps.Marker({
            position: {lat: $embed.data('lat'), lng: $embed.data('lng')},
            animation: google.maps.Animation.DROP,
            icon: icon,
            map: map
        });

        // place all places markers
        var counter, lastGroup = null;
        $(container).find(this.ui.place).each(function(){
            // reset counter, if new group
            if ($(this).data('group') != lastGroup){
                counter = 1;
            }
            // create marker
            var marker = new google.maps.Marker({
                position: {lat: $(this).data('lat'), lng: $(this).data('lng')},
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    fillOpacity: 1,
                    scale: 12
                },
                label: {text: counter.toString(), color: "white"},
                map: map,
                visible: false,
                infowindow: locationInfowindow
            });

            $(this).data('marker', marker);

            //keep track of markers for infowindow hiding
            markers.push(marker);

            // count
            lastGroup = $(this).data('group');
            counter++;

            //create infowindow content
            var contentString = '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<h1 id="firstHeading" class="firstHeading">' + $(this).data('name')+ '</h1>'+
                '<div id="bodyContent">'+
                '<p>' + $(this).data('description')+ '</p>'+
                '</div>'+
                '</div>';

            //create infowindow
            marker.locationInfowindow = new google.maps.InfoWindow({
                content: contentString
            });

            //infowidow listener
            google.maps.event.addListener(marker, 'click', function() {
                hideAllInfoWindows(map);
                marker.locationInfowindow.open(map, this);
            });

        });

        //infowindow hiding function
        function hideAllInfoWindows(map) {
            markers.forEach(function(marker) {
                marker.locationInfowindow.close(map, marker);
            });
        }

        // bind events
        $(container).on('click', this.ui.group, function(e){
            hideAllInfoWindows(map);
            var $group = $(this);
            setTimeout(function(){
                var visible = $group.attr('aria-expanded') == 'true';
                $(container).find(self.ui.place).each(function(){
                    if (visible && $(this).is('[data-group="' + $group.data('id') + '"]')){
                        $(this).data('marker').setVisible(true);
                    }else{
                        $(this).data('marker').setVisible(false);
                    }
                });
            },10);
        });
        $(container).on('click', this.ui.place, function(e){
            e.preventDefault();
            map.panTo($(this).data('marker').getPosition());
        });

    }
};

$(function(){
    EmbedMap.ready();
});