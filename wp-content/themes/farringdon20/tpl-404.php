<?php
$pageTitle = 'HB Reavis - 20Farringdon';
$pageClass = 'page404';
$pageName = 'page404';
?>
<?php include('tpl-inc/head.php'); ?>

<div id="page" class="<?php print $pageClass; ?>">

    <?php include('tpl-inc/cookies.inc.php'); ?>
    <?php include('tpl-inc/header.php'); ?>

    <main id="main">

        <div class="section-top">
            <div class="section-inner">
                <div class="section-wrap">
                    <div class="error-wrap">
                        <h1 class="title"><span class="t">404</span></h1>
                        <div class="text">
                            <p>We couldn't find the page<br> you are looking for.</p>
                        </div>
                        <a href="/" class="el-btn">Go Home</a>
                    </div>
                </div>
            </div>
        </div>

    </main>

    <?php include('tpl-inc/footer.php'); ?>

</div>

<?php include('tpl-inc/foot.php'); ?>