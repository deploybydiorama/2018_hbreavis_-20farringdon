<?php
$pageTitle = 'HB Reavis - 20Farringdon';
$pageClass = 'front';
$pageName = 'front';
?>
<?php include('tpl-inc/head.php'); ?>

<div id="page" class="<?php print $pageClass; ?>">

    <?php include('tpl-inc/cookies.inc.php'); ?>
    <?php include('tpl-inc/header.php'); ?>

    <main id="main">

        <div class="section-top">
            <div class="section-inner">
                <div class="section-wrap">
                    <div class="text-wrap">
                        <h1 class="title"><span class="t">BRIGHTER <br>IDEAS</span></h1>
                        <div class="text">
                            <p>20 Farringdon Street is more than an office. More than a workspace. It’s an environment tailored for the modern professional.</p>
                        </div>
                        <a href="/" class="el-btn">More</a>
                    </div>
                    <div class="image-wrap">
                        <span class="image" style="background-image:url('public/i/img01.png');"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-text-left section-front-intro">
            <div class="section-inner">
                <div class="row">
                    <div class="text-wrap">
                        <h2 class="el-section-title">20 FARRINGDON STREET</h2>
                        <p>An 85,000 sq ft environment, spread across 11 column-free floors with unexpected terrace views and a heady mix of financial, media, legal and tech institutions for neighbours. Most crucially, however, it also comes with baked-in wellbeing features that make working life an absolute joy.</p>
                        <a href="/" class="el-readmore">EXPLORE THE BUILDING</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-slider section-front-slider">
            <div class="section-bg">
                <div class="section-inner">
                    <div class="slider">
                        <div class="slider-item"><a href="/"><span class="image" style="background-image:url('public/i/img-slide01.jpg');"></span></a></div>
                        <div class="slider-item"><a href="/"><span class="image" style="background-image:url('public/i/img-slide01.jpg');"></span></a></div>
                        <div class="slider-item"><a href="/"><span class="image" style="background-image:url('public/i/img-slide01.jpg');"></span></a></div>
                        <div class="slider-item"><a href="/"><span class="image" style="background-image:url('public/i/img-slide01.jpg');"></span></a></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-articles">
            <div class="section-inner">
                <div class="row">
                    <div class="col-md-6 offset-md-6">
                        <h2 class="el-section-title">WHAT MAKES 20 FARRINGDON <br>STREET OUTSTANDING</h2>
                    </div>
                </div>
                <div class="articles row">
                    <div class="col-md-4">
                        <article class="article-item" data-link="/">
                            <a href="/" class="image"><img src="public/i/img-news02.jpg" alt=""></a>
                            <h3 class="title"><a href="/">RECEPTION</a></h3>
                            <div class="text">
                                <p>With a double height ceiling, facial recognition security and quality; the lobby feels like a bustling business lounge. Complete with a striking bespoke feature light that flows on from reception, and up 20 Farringdon Street’s feature stairs.</p>
                            </div>
                            <a href="/" class="el-readmore">EXPLORE THE BUILDING</a>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="article-item" data-link="/">
                            <a href="/" class="image"><img src="public/i/img-news02.jpg" alt=""></a>
                            <h3 class="title"><a href="/">LIGHT FOOTED</a></h3>
                            <div class="text">
                                <p>The bespoke lighting leads through reception and follows a path up the double width polished concrete staircase. A guide through the building, the feature also provides an illuminating daily exercise to strengthen body and soul.</p>
                            </div>
                            <a href="/" class="el-readmore">EXPLORE THE BUILDING</a>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="article-item" data-link="/">
                            <a href="/" class="image"><img src="public/i/img-news02.jpg" alt=""></a>
                            <h3 class="title"><a href="/">SKYLINE SIGHTS</a></h3>
                            <div class="text">
                                <p>20 Farringdon street’s upper floors showcase London’s most famous architecture. A backdrop that inspires whether working at the desk, sat on a lunch room sofa or chatting during an after-hours get together.</p>
                            </div>
                            <a href="/" class="el-readmore">EXPLORE THE BUILDING</a>
                        </article>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-text-left section-front-creative">
            <span class="bg-triangle"></span>
            <div class="section-inner">
                <div class="row">
                    <div class="text-wrap">
                        <h2 class="el-section-title">AMBITIOUSLY CREATIVE</h2>
                        <p>Farringdon is synonymous with technology and creativity. But with the City of London financial district just yards away, there’s also an undercurrent of astute professionalism.</p>
                        <a href="/" class="el-readmore">EXPLORE THE BUILDING</a>
                    </div>
                    <div class="diamonds-items">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-text">
                                    <span class="num">32</span>
                                    <span class="t">TRAVEL TIME TO HEATHROW AIRPORT IS ONLY 32 MINUTES BY PUBLIC TRANSPORT</span>
                                </span>
                            </span>
                        </a>
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-text">
                                    <span class="num">24</span>
                                    <span class="t">BARS AND RESTAURANTS UNDER 5 MINUTES</span>
                                </span>
                            </span>
                        </a>
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-text">
                                    <span class="num">142</span>
                                    <span class="t">RACKS FOR BIKES, HANDY LOCKERS AND INVITING SHOWERS</span>
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-location">
            <div class="section-inner">
                <div class="row align-items-end">
                    <div class="col-md-8 map-wrap">
                        <div class="map-inner">
                            <img src="public/i/img-map.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-4 opt-wrap">
                        <a class="item"><img src="public/i/img-location01.jpg" alt=""><span class="t">EXPLORE <br>CONNECTIONS</span></a>
                        <a class="item"><img src="public/i/img-location01.jpg" alt=""><span class="t">EXPLORE <br>THE NEIGHBOURHOOD</span></a>
                    </div>
                </div>
            </div>
        </div>

        <?php include('tpl-inc/inc-section-contact.php'); ?>

    </main>

    <?php include('tpl-inc/footer.php'); ?>

</div>

<?php include('tpl-inc/foot.php'); ?>