<?php
/* Template Name: Location */
require_once 'app/KmlReader.php';

add_filter('timber_context', function($context) use ($app){

    // TODO, parse the online file
    $kml = new KmlReader(__DIR__ . '/app/config/kml/map.kml');
    $context['map'] = (object) [
        'groups' => $kml->getGroups(),
        'amenities' => [
            'bars-restaurants',
            'retail',
            'hotels',
            'transport',
            'green-space',
        ],
        'occupiers' => [
            'corporate',
            'financial',
            'legal',
            'tech-media-telecommunication',
        ],
    ];

    return $context;

});

$app->render('location.latte');