<?php

use Tracy\Debugger;

require_once get_template_directory() . '/vendor/autoload.php';
require_once get_template_directory() . '/app/download.php';

// init
Debugger::enable(['95.105.208.226', '92.240.245.151'], __DIR__ . '/log');
//Debugger::$showBar = false;
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);

// templates setup
$timber = new \Timber\Timber();
Timber::$dirname = 'app/templates';

// create site
$app = require 'app/App.php';

// enable temp directory?
// not supported on WPEngine
//$app->setTempDirectory(get_template_directory() . '/temp/latte');