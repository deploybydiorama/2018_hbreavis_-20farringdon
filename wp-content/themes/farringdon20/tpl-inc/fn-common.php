<?php
require_once('fn-lorem-ipsum.php');
function fnArticleTeaser($link='/',$size=""){
    $lipsum = new joshtronic\LoremIpsum();
    $x = $lipsum->words(2);
    $img = 'public/i/img0'.rand(1,6).'.jpg';
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // nezabudni pls pridat do <img> atribut: srcset="...2x" (+ zoptimalizovat rozlisenie obrazkov pre normal a large teaser)
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    print '
        <article class="el-article-teaser '.$size.'" data-link="'.$link.'">
            <div class="image-wrap"><a href="'.$link.'"><img src="'.$img.'" alt=""></a></div>
            <div class="text-wrap">
                <div class="subtitle">'.$lipsum->words(2).'.</div>
                <h2 class="title">'.$lipsum->words(rand(4,8)).'.</h2>
                <div class="text">'.$lipsum->words(rand(20,25)).'.</div>
            </div>
        </article>';
}
function fnArticleData(){
    print '
    <div class="el-article-data">
        <div class="data-wrap">
            <a href="/" class="author">Blockchain tech</a>
            <span class="date">29. 03. 2018</span>
        </div>
        <div class="share-wrap">
            <span class="share-label">Share:</span>
            <a href="/" class="share-link">Facebook</a>
            <a href="/" class="share-link">Twitter</a>
            <a href="/" class="share-link">LinkedIn</a>
        </div>
    </div>
    ';
}
function fnBlockImageText($image,$text){
    print '
    <div class="el-block-image-text">
        <div class="image-wrap"><span class="image" style="background-image:url(\''.$image.'\');"></span></div>
        <div class="text-wrap">'.$text.'</div>
    </div>
    ';
}
function fnNewsTeaser(){
    $lipsum = new joshtronic\LoremIpsum();
    $x = $lipsum->words(2);
    $imgNum = rand(0,6);
    $image = '';
    if($imgNum!=0){
        $image = '<div class="image-wrap"><a href="/"><img src="public/i/img0'.$imgNum.'.jpg" alt=""></a></div>';
    }
    $likes = (rand(0,50) * 5);
    $networks =array('facebook','instagram','twitter');
    $network = $networks[rand(0,(count($networks)-1))];
    print '
    <article class="el-news-teaser" data-link="/">
        '.$image.'
        <div class="text-wrap">
            <div class="text">'.$lipsum->words(rand(15,35)).'.</div>
            <div class="social">
                <div class="stats">
                    <span class="i"><i class="icon-comment"></i><span class="t">'.$likes.'</span></span>
                    <span class="i"><i class="icon-thumbs-up"></i><span class="t">'.$likes.'</span></span>
                </div>
                <div class="source">
                    <i class="icon-'.$network.'"></i>
                </div>
            </div>
        </div>
    </article>';
}
function fnTeamMemeber($img=null,$name=null,$position=null,$textShort=null,$textLong=null){
    print '<article class="el-team">';
    if (isset($img)) {
        print '<div class="image-wrap"><a href="/"><img src="'.$img.'" alt=""></a></div>';
    }
    print '<div class="title-wrap do-match-height">';
    if (isset($name)) {
        print '<h3 class="name">'.$name.'</h3>';
    }
    if (isset($position)){
        print '<div class="position">'.$position.'</div>';
    }
    print '</div>';//title-wrap
    if (isset($textShort) && isset($textLong)){
        print '<div class="text"><span class="text-short">'.$textShort.'</span><span class="text-long">'.$textLong.'</span><a class="btn-toggle" role="button"></a></div>';
    }
    print '</article>';
}