<div class="section-contact">
    <div class="section-bg"></div>
    <div class="section-inner">
        <div class="contacts-wrap">
            <h3 class="title">Contact Details</h3>
            <div class="items">
                <div class="item formated-output inv">
                    <h4>KNIGHT FRANK</h4>
                    <p><strong>Angus Goswell</strong><br>
                        <a href="tel:+44 7717 343 151">+44 7717 343 151</a><br>
                        <a href="mailto:angus.goswell@knightfrank.com">angus.goswell@knightfrank.com</a></p>
                    <p><strong>Abby Brown</strong><br>
                        <a href="tel:+44 7827 083 969">+44 7827 083 969</a><br>
                        <a href="mailto:abby.brown@knightfrank.com">abby.brown@knightfrank.com</a></p>
                    <p><strong>Will Foster</strong><br>
                        <a href="tel:+44 7748 985 951">+44 7748 985 951</a><br>
                        <a href="mailto:william.foster@knightfrank.com">william.foster@knightfrank.com</a></p>
                </div>
                <div class="item formated-output inv">
                    <h4>CUSHMAN &amp; WAKEFIELD</h4>
                    <p><strong>James Oliver</strong><br>
                        <a href="tel:+44 7771 815 144">+44 7771 815 144</a><br>
                        <a href="mailto:james.oliver@cushwake.com">james.oliver@cushwake.com</a></p>
                    <p><strong>James Campbell</strong><br>
                        <a href="tel:+44 7738 737 366">+44 7738 737 366</a><br>
                        <a href="mailto:james.campbell@cushwake.com">james.campbell@cushwake.com</a></p>
                    <p><strong>Brittany Corr</strong><br>
                        <a href="tel:+44 7793 808 379">+44 7793 808 379</a><br>
                        <a href="mailto:brittany.corr@eur.cushwake.com">brittany.corr@eur.cushwake.com</a></p>
                </div>
                <div class="item formated-output inv">
                    <h4>HB REAVIS</h4>
                    <p><strong>Charlie Russell-Jones</strong><br>
                        <a href="tel:+44 7825 586 587">+44 7825 586 587</a><br>
                        <a href="mailto:charlie.russell-jones@hbreavis.com">charlie.russell-jones@hbreavis.com</a></p>
                </div>
            </div>
        </div>
        <form class="form-wrap hf-form-29">
            <div class="row">
                <div class="col-sm-6">
                    <div class="pb-3">
                        <input type="text" placeholder="Company name" class="el-field-text">
                    </div>
                    <div class="pb-3">
                        <input type="text" placeholder="Your name" class="el-field-text">
                    </div>
                    <div class="pb-3">
                        <input type="email" placeholder="Email" class="el-field-text">
                    </div>
                    <div class="pb-3 pb-sm-13">
                        <input type="phone" placeholder="Mobile number" class="el-field-text">
                    </div>
                    <div class="pb-3">
                        <p>Required area in sq ft:</p>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <input type="text" placeholder="From" class="el-field-text mb-3 mt-sm-0">
                        </div>
                        <div class="col-xs-6">
                            <input type="text" placeholder="To" class="el-field-text mb-3 mt-sm-0">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <textarea placeholder="Message" class="el-field-textarea"></textarea>
                </div>
            </div>
            <div class="row justify-content-end">
                <div class="col-auto pt-6">
                    <button type="submit" class="el-field-submit">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>