<header id="header">
    <div class="inner">
        <div class="header-panel">
            <div class="logo-wrap">
                <a href="tpl-index.php" class="logo"><img src="public/i/logo-h.svg" alt="logo" class="logo-mobil"><img src="public/i/logo-v.svg" alt="logo" class="logo-desktop"></a>
            </div>
            <div class="social-wrap">
                <a href="/"><i class="icon-facebook"></i></a>
                <a href="/"><i class="icon-linkedin-in"></i></a>
                <a href="/"><i class="icon-twitter"></i></a>
                <a href="/"><i class="icon-instagram"></i></a>
            </div>
        </div>
        <a class="toggle-nav"><span class="burger"></span></a>
        <nav class="main-nav">
            <a href="tpl-index.php" class="item active">HOME</a>
            <a href="tpl-location.php" class="item">LOCATION</a>
            <a href="tpl-building.php" class="item">BUILDING</a>
            <a href="tpl-contact.php" class="item">CONTACT</a>
            <span class="spacer"></span>
            <a href="/" class="item">DOWNLOAD</a>
            <span class="social">
                <a href="/"><i class="icon-facebook"></i></a>
                <a href="/"><i class="icon-linkedin-in"></i></a>
                <a href="/"><i class="icon-twitter"></i></a>
                <a href="/"><i class="icon-instagram"></i></a>
            </span>
        </nav>
    </div>
</header>
<div id="header-placeholder"></div>