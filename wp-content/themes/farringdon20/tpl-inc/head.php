<?php
include('fn-common.php');
?>
<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width, target-densityDpi=160dpi">
    <!--[if (IEMobile)]><meta http-equiv="cleartype" content="on"><![endif]-->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <title><?php print $pageTitle; ?></title>
    <!--link rel="shortcut icon" href="../www/etc/favicon.ico" type="image/x-icon"-->
    <link rel="icon" href="public/i/favico.png" type="image/png" />
    <link rel="stylesheet" href="https://use.typekit.net/fdp2nxv.css">
    <link href="public/css/main.min.css" rel="stylesheet" type="text/css" />
</head>
<body>