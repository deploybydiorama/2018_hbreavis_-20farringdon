<footer id="footer">
    <div class="inner">
        <div class="inner-wrap">
            <div class="hb-reavis">
                <span class="text">A new development by</span>
                <a href="https://hbreavis.com/en/" target="_blank" class="logo"><img src="public/i/logo-hbreavis.svg" alt=""></a>
            </div>
            <div class="copyright">
                &copy; 2018 – 20 FARRINGDON STREET
            </div>
        </div>
        <div class="dislacimer-wrap">
            <div class="dislacimer">Spoločnosť vynaložila primeranú starostlivosť na to, aby zabezpečila, že informácie uvedené na tejto webovej stránke (nie informácie na hypertextových prepojeniach) boli presné v čase ich poslednej aktualizácie. Spoločnosť nenesie zodpovednosť za presnosť, úplnosť alebo dôsledky vyplývajúce z používania informácií uvedených na tejto webovej stránke a takisto nemá povinnosť ich aktualizovať. Tieto informácie si nemožno vykladať ako rady alebo odporúčania, na ktorých základe by ste mali alebo nemali vykonávať rozhodnutia alebo opatrenia. Skutočné výsledky alebo vývoj sa môžu podstatne odlišovať od prognóz, stanovísk alebo očakávaní uvedených na tejto webovej stránke. Niektoré informácie na tejto webovej stránke majú historický charakter a nemusia byť aktuálne. Všetky historické informácie je nutné považovať za aktuálne v dátume ich prvého zverejnenia. Nič na tejto webovej stránke si nemožno vykladať ako výzvu alebo ponuku na investovanie alebo obchodovanie s cennými papiermi Spoločnosti. Táto webová stránka obsahuje aj hypertextové prepojenia na iné webové stránky. Spoločnosť nemá pod kontrolou a nenesie žiadnu zodpovednosť za akékoľvek informácie alebo stanoviská uvedené na iných webových stránkach.</div>
        </div>
    </div>
</footer>