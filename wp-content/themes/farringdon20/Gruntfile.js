module.exports = function (grunt) {

    // autoloads npm tasks
    require('load-grunt-tasks')(grunt);
    
    const sass = require('node-sass');

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            images: {
                files: ['assets/i/**/*.{svg,gif,png,jpg,jpeg}'],
                tasks: ['images']
            },
            js: {
                files: ['assets/js/**/*.js'],
                tasks: ['js']
            },
            css: {
                files: ['assets/scss/**/*.scss'],
                tasks: ['css']
            },
            livereload: {
                files: ['public/js/*.js', 'public/css/*.css'],
                options: {livereload: true}
            }
        },
        // ****************** JS ******************
        concat: {
            options: {
                sourceMap: true
            },
            js: {
                src: [
                    'bower_components/jquery/dist/jquery.js',
                    'bower_components/slick-carousel/slick/slick.min.js',
                    'bower_components/bootstrap/js/dist/util.js',
                    'bower_components/bootstrap/js/dist/tab.js',
                    'bower_components/bootstrap/js/dist/collapse.js',
                    'bower_components/underscore/underscore.js',
                    'assets/js/main/**/*.js'
                ],
                dest: 'temp/grunt/js/main.js'
            },
            css: {
                src: [
                    'temp/grunt/css/compiled/main/**/*.css'
                ],
                dest: 'temp/grunt/css/merged/main/main.css'
            }
        },
        babel: {
            options: {
                sourceMap: true,
                plugins: ['transform-react-jsx'],
                presets: ['es2015', 'react']
            },
            main: {
                src: [
                    'temp/grunt/js/main.js'
                ],
                dest: 'temp/grunt/js/main.es5.js'
            }
        },
        uglify: {
            options: {
                sourceMap: true,
                sourceMapIn: function(file){
                    return file + '.map';
                }
            },
            main: {
                src: [
                    'temp/grunt/js/main.js'
                ],
                dest: 'public/js/main.min.js'
            }
        },
        // ****************** CSS ******************
        sass: {
            options: {
                sourceMap: false,
                outputStyle: 'compressed',
                implementation: sass
            },
            main: {
                expand: true,
                cwd: 'assets/scss/main/',
                src: '**/*.scss',
                dest: 'temp/grunt/css/compiled/main',
                ext: '.css'
            },
            login: {
                expand: true,
                cwd: 'assets/scss/login/',
                src: '**/*.scss',
                dest: 'temp/grunt/css/compiled/login',
                ext: '.css'
            }
        },
        autoprefixer: {
            options: {
                map: true
            },
            main: {
                expand: true,
                cwd: 'temp/grunt/css/merged/main',
                src: '**/*.css',
                dest: 'temp/grunt/css/autoprefixed/main'
            }
        },
        cssmin: {
            options: {
                keepSpecialComments: 0,
                sourceMap: true,
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            main: {
                files: {
                    'public/css/main.min.css': [
                        'temp/grunt/css/autoprefixed/main/**/*.css'
                    ]
                }
            },
            login: {
                files: {
                    'public/css/login.min.css': [
                        'temp/grunt/css/compiled/login/**/*.css'
                    ]
                }
            }
        },
        // ****************** IMAGES ******************,
        copy: {
            images: {
                files: [{
                    expand: true,
                    cwd: 'assets/i/',
                    src: ["**/*.{svg,gif}"],
                    dest: "public/i/"
                }]
            }
        },
        tinypng: {
            options: {
                apiKey: '23V-nuBTBtyMPaRsK6Fs_cBjwc7bfoRe',
                checkSigs: true,
                sigFile: 'assets/i/.tinypng.sigfile',
                sigFileSpace: 4,
                summarize: true,
                showProgress: true
            },
            images: {
                files: [{
                    expand: true,
                    cwd: 'assets/i/',
                    src: ["**/*.{png,jpg,jpeg}"],
                    dest: "public/i/"
                }]
            }
        },
        // ****************** INSTALL ******************,
        shell: {
            composer: {
                command: 'composer install'
            },
            bower: {
                command: 'node node_modules/bower/bin/bower install'
            }
        }
    });

    // register tasks
    grunt.registerTask('images', ['copy', 'tinypng']);
    grunt.registerTask('css', ['sass', 'concat:css', 'autoprefixer', 'cssmin']);
    grunt.registerTask('js', ['concat:js', 'uglify']);
    grunt.registerTask('dependencies', ['shell:composer', 'shell:bower']);
    //grunt.registerTask('default', ['dependencies', 'images', 'css', 'js']);
    grunt.registerTask('default', ['images', 'css', 'js']);

};