<?php

require_once 'LatteMacroSet.php';

class App extends TimberSite
{

    protected $tempDir = null;

    protected $recaptcha = [
        'key' => '6LfGbtkZAAAAAE5xxGhr9Tqd7ApjW7wpHV1rfNCh',
        'secret' => '6LfGbtkZAAAAAG5cF-6U3IsdBFZPrRIOyMEFvlzn',
    ];

    function __construct()
    {

        // theme setup
        require get_template_directory() . '/vendor/pavoleichler/wordpress-configurator/src/wordpress_configurator.php';
		
		// insightly leads
        require get_template_directory() . '/app/DeployInsightly.php';

        $configurator = new \WordpressConfigurator\WordpressConfigurator('farringdon20');
        $configurator->addFile(get_template_directory() . '/app/config/theme.neon', new \WordpressConfigurator\Handlers\Theme\ThemeHandler());
        $configurator->addFile(get_template_directory() . '/app/config/plugins.neon', new \WordpressConfigurator\Handlers\Plugins\PluginsHandler());
        $configurator->addFolder(get_template_directory() . '/app/config/acf', new \WordpressConfigurator\Handlers\ACF\ACFHandler());
        $configurator->addFolder(get_template_directory() . '/app/config/acf/options', new \WordpressConfigurator\Handlers\ACF\ACFOptionsHandler());
        $configurator->addFolder(get_template_directory() . '/app/config/acf/pages', new \WordpressConfigurator\Handlers\ACF\ACFPageHandler());

        // template hooks
        add_filter('timber_context', [$this, 'registerContext']);

        // assets
        add_action( 'wp_enqueue_scripts', [$this, 'registerAssets']);

        // login screen
        add_action( 'login_enqueue_scripts', function(){
            wp_enqueue_style( 'login-style', get_template_directory_uri() . '/public/css/login.min.css', false , @filemtime(get_template_directory() . '/public/css/login.min.css'),'all');
        });

        // ACF init
        add_action('acf/init', function(){
            acf_update_setting('google_api_key', get_field('google_maps_key', 'option'));
        });

        // Google Recaptcha form protection
        add_action('hf_process_form', function( &$form, &$submission ) {
            $recaptcha = new \ReCaptcha\ReCaptcha($this->recaptcha['secret']);
            $result = $recaptcha->verify($submission->data['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
            $submission->data['recaptcha-score'] = $result->getScore();
            if (!$result->isSuccess()){
                // this is spam
                // disable all actions
                $form->settings['actions'] = null;
                // mark as spam
                $submission->data['SPAM'] = true;
            }
        }, 10, 2);

		// html form plugin submit
        add_action( 'hf_form_success', function( $submission, $form ) {

            // ignore spam submission
            if ($submission->data['SPAM']){
                return;
            }

            if($form->slug === "contact"){

                $submission_data = $submission->data;
                $name = $submission_data['NAME'];
                $name_array = explode(" ", $name);
                $first_name = $name_array[0];
                $last_name = "-";
                if($name_array[1]){
                    $last_name = $name_array[1];
                }

                $email = $submission_data['EMAIL'];
                $phone = $submission_data['PHONE'];
                $message = $submission_data['MESSAGE'];
                
                $data = [
                    'FIRST_NAME'    => $first_name,
                    'LAST_NAME'     => $last_name,
                    'EMAIL_ADDRESS' => $email,
                    'PHONE'         => $phone,
                    'CUSTOMFIELDS'  => [
                        [
                            'FIELD_NAME'  => 'Message__c',
                            'FIELD_VALUE' => $message
                        ]
                    ]
                ];

                $id = DeployInsightly::create_contact($data);
                DeployInsightly::create_contact_tag($id, '20FarringdonContactform');
            }

        }, 10, 2);

        //change password to wp-admin message
        add_filter("retrieve_password_message", "mapp_custom_password_reset", 99, 4);

        function mapp_custom_password_reset($message, $key, $user_login, $user_data ){

            $message = __( 'Someone has requested a password reset for the following account:' ) . "\r\n\r\n";
            /* translators: %s: user login */
            $message .= sprintf( __( 'Username: %s'), $user_login ) . "\r\n\r\n";
            $message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
            $message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
            $message .= ' ' . network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . "\r\n";


            return $message;
        }

        parent::__construct();

    }

    public function registerAssets(){

        wp_enqueue_style( 'main-style', get_template_directory_uri() . '/public/css/main.min.css',false, @filemtime(get_template_directory() . '/public/css/main.min.css'),'all');
        wp_enqueue_script( 'main-script', get_template_directory_uri() . '/public/js/main.min.js',false, @filemtime(get_template_directory() . '/public/js/main.min.js'),true);

        wp_localize_script( 'main-script', 'option', array(
            'google_maps_key' => get_field('google_maps_key', 'option'),
        ));

        wp_localize_script( 'main-script', 'link', array(
            'themePath' => get_stylesheet_directory_uri(),
        ));

    }

    public function registerContext($context)
    {

        // this
        $context['site'] = $this;
        // ACF
        $context['options'] = get_fields('option');
        // theme directory
        $context['themeDir'] = get_template_directory();
        // paths
        $context['basePath'] = get_site_url();
        $context['baseUrl'] = get_site_url();
        $context['themePath'] = $this->theme->link;
        // post and fields
        $context['fields'] = get_fields();
        $context['post'] = new TimberPost();
        // recaptcha
        $context['recaptchaSiteKey'] = $this->recaptcha['key'];

        return $context;

    }

    public function setTempDirectory($tempDir)
    {

        $this->tempDir = $tempDir;

        if ($tempDir and !is_dir($tempDir)){
            mkdir($tempDir, 0777, true);
        }

    }

    public function render($file)
    {

        // create latte engine
        $latte = new \Latte\Engine();

        // temp dir
        // Disable for wpengine
        if ($this->tempDir){
            $latte->setTempDirectory($tempDir);
        }

        // register latte macros
        LatteMacroSet::install($latte->getCompiler());

        // render
        $context = Timber::get_context();
        $latte->render(get_template_directory() . '/' . Timber::$dirname . '/' . $file, $context);

    }

}

return new App();