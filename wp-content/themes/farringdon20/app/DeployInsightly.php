<?php
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class DeployInsightly {

    private static $insightly_api_url = INSIGHTLY_API_URL;
    private static $insightly_api_key = INSIGHTLY_API_KEY;
    private static $insightly_lead_log_name = INSIGHTLY_LEAD_LOG_NAME;
    private static $insightly_contact_log_name = INSIGHTLY_CONTACT_LOG_NAME;
    
    public static function create_lead($data) {

        $client = new \GuzzleHttp\Client();

        try {
            $response = $client->request('POST', self::$insightly_api_url . "/leads", [
                'headers' => [
                    'Authorization'     => 'Basic ' . base64_encode(self::$insightly_api_key),
                    'Accept-Encoding'   => 'gzip',
                    'Content-Type'      => 'application/json'
                ],
                'body' => json_encode($data)
            ]);
            
            $body = $response->getBody();
            $lead = json_decode($body);
            $id = $lead->LEAD_ID;

            if( defined(INSIGHTLY_CONTACT_LOG_NAME) && self::$insightly_lead_log_name !== ""){
                $log = new Logger('Insightly/Lead');
                $log->pushHandler(new StreamHandler(WP_CONTENT_DIR . '/' . self::$insightly_lead_log_name, Logger::INFO));
                $log->info('Lead successfully created', ['id' => $id]);
            }

            return $id;

        }
        catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBody = $response->getBody()->getContents();

            if(defined(INSIGHTLY_CONTACT_LOG_NAME) && self::$insightly_lead_log_name !== ""){
                $log = new Logger('Insightly/Lead');
                $log->pushHandler(new StreamHandler(WP_CONTENT_DIR . '/' . self::$insightly_lead_log_name, Logger::WARNING));
                $log->error('Lead dont created', ['response' => $responseBody, 'status_code' => $response->getStatusCode()]);
            }
        }
    } 

    public static function create_contact($data) {

        $client = new \GuzzleHttp\Client();

        try {
            $response = $client->request('POST', self::$insightly_api_url . "/contacts", [
                'headers' => [
                    'Authorization'     => 'Basic ' . base64_encode(self::$insightly_api_key),
                    'Accept-Encoding'   => 'gzip',
                    'Content-Type'      => 'application/json'
                ],
                'body' => json_encode($data)
            ]);
            
            $body = $response->getBody();
            $contact = json_decode($body);
            $id = $contact->CONTACT_ID;

            if(defined(INSIGHTLY_CONTACT_LOG_NAME) && self::$insightly_contact_log_name !== ""){
                $log = new Logger('Insightly/Contact');
                $log->pushHandler(new StreamHandler(WP_CONTENT_DIR . '/' . self::$insightly_contact_log_name, Logger::INFO));
                $log->info('Contact successfully created', ['id' => $id]);
            }

            return $id;
        }
        catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBody = $response->getBody()->getContents();

            if(defined(INSIGHTLY_CONTACT_LOG_NAME) && self::$insightly_contact_log_name !== ""){
                $log = new Logger('Insightly/Contact');
                $log->pushHandler(new StreamHandler(WP_CONTENT_DIR . '/' . self::$insightly_contact_log_name, Logger::WARNING));
                $log->error('Contact dont created', ['response' => $responseBody, 'status_code' => $response->getStatusCode()]);
            }
        }
        
    }

    public static function create_contact_tag($id, $tag_name) {
		
		$client = new \GuzzleHttp\Client();
		
        try {

            $data = [
                "TAG_NAME" => $tag_name
            ];

            $client = new \GuzzleHttp\Client();

            $response = $client->request('POST', self::$insightly_api_url . '/contacts/' . $id . '/tags', [
                'headers' => [
                    'Authorization'     => 'Basic ' . base64_encode(self::$insightly_api_key),
                    'Accept-Encoding'   => 'gzip',
                    'Content-Type'      => 'application/json'
                ],
                'body' => json_encode($data)
            ]);
            
            $body = $response->getBody();

            if(defined(INSIGHTLY_CONTACT_LOG_NAME) && self::$insightly_contact_log_name !== ""){
                $log = new Logger('Insightly/ContactTag');
                $log->pushHandler(new StreamHandler(WP_CONTENT_DIR . '/' . self::$insightly_contact_log_name, Logger::INFO));
                $log->info('Contact tag successfully created', ['id' => $id]);
            }
        }
        catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBody = $response->getBody()->getContents();

            if(defined(INSIGHTLY_CONTACT_LOG_NAME) && self::$insightly_contact_log_name !== ""){
                $log = new Logger('Insightly/ContactTag');
                $log->pushHandler(new StreamHandler(WP_CONTENT_DIR . '/' . self::$insightly_contact_log_name, Logger::WARNING));
                $log->error('Contact tag dont created', ['response' => $responseBody, 'status_code' => $response->getStatusCode()]);
            }
        }
    }
}