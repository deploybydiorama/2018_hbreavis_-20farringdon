<?php
/**
 * Created by PhpStorm.
 * User: pavol
 * Date: 22. 3. 2018
 * Time: 21:11
 */

class LatteMacroSet extends \Latte\Macros\MacroSet
{

    public static function install(Latte\Compiler $compiler)
    {
        $set = new static($compiler);
        $set->addMacro('link', [$set, 'link']);
        $set->addMacro('href', null, null, [$set, 'href']);
    }

    public function link(\Latte\MacroNode $node, \Latte\PhpWriter $writer)
    {

        $pages = get_pages([
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-' . $node->args  . '.php',
            'hierarchical' => 0
        ]);

        // manage languages
        if (count($pages)){
            if ($postId = function_exists('pll_get_post') ? pll_get_post($pages[0]->ID, pll_current_language()) : $pages[0]->ID){
                $href = get_permalink($postId);
            }else{
                $href = '#';
            }
        }else{
            $href = '#';
        }

        return $writer->write('echo \'href="' . preg_replace("/'/", "\\'", $href) . '"\';');

    }

    public function href(\Latte\MacroNode $node, \Latte\PhpWriter $writer)
    {

        $pages = get_pages([
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-' . $node->args  . '.php',
            'hierarchical' => 0
        ]);

        // manage languages
        if (count($pages)){
            if ($postId = function_exists('pll_get_post') ? pll_get_post($pages[0]->ID, pll_current_language()) : $pages[0]->ID){
                $href = get_permalink($postId);
            }else{
                $href = '#';
            }
        }else{
            $href = '#';
        }

        return $writer->write('echo \'href="' . preg_replace("/'/", "\\'", $href) . '"\';');
    }

}