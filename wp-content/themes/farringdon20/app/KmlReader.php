<?php
/**
 * Created by PhpStorm.
 * User: pavol
 * Date: 22. 3. 2018
 * Time: 21:11
 */

class KmlReader
{

    /**
     * @var \StepanDalecky\KmlParser\Parser
     */
    protected $parser;
    protected $data = [];

    public function __construct($file)
    {

        $this->parser = \StepanDalecky\KmlParser\Parser::fromFile($file);
        $this->data = $this->parse($this->parser);

    }

    protected function parse($parser)
    {

        $folders = $parser->getKml()->getDocument()->getFolders();

        $data = [];
        foreach($folders as $folder){
            $data[] = $this->parseFolder($folder);
        }

        return $data;

    }

    protected function parseFolder(\StepanDalecky\KmlParser\Entities\Folder $folder)
    {

        $places = [];
        foreach($folder->getPlacemarks() as $placemark){
            try {
                $places[] = $this->parsePlacemark($folder, $placemark);
            }catch(Exception $exc){
                // skip invalid places
            }
        }

        return (object) [
            'name' => $folder->getName(),
            'id' => \Nette\Utils\Strings::webalize($folder->getName()),
            'places' => $places
        ];
    }

    protected function parsePlacemark(\StepanDalecky\KmlParser\Entities\Folder $folder, \StepanDalecky\KmlParser\Entities\Placemark $placemark)
    {

        preg_match('/^[\s]*(.*?),(.*?)[\s]*(,|$)/', $placemark->getPoint()->getCoordinates(), $coordinates);

        return (object) [
            'id' => \Nette\Utils\Strings::webalize($folder->getName() . '_' . $placemark->getName()),
            'group' => \Nette\Utils\Strings::webalize($folder->getName()),
            'name' => $placemark->getName(),
            'description' => $placemark->getDescription(),
            'lat' => $coordinates[2],
            'lng' => $coordinates[1],
        ];

    }

    public function getGroups()
    {

        return $this->data;

    }


    public function getMarkers()
    {

        $markers = [];
        foreach($this->data as $group){
            $markers = array_merge($markers, $group->places);
        }

        return $markers;

    }

}