<?php
$pageTitle = 'HB Reavis - 20Farringdon';
$pageClass = 'location';
$pageName = 'location';
?>
<?php include('tpl-inc/head.php'); ?>

<div id="page" class="<?php print $pageClass; ?>">

    <?php include('tpl-inc/cookies.inc.php'); ?>
    <?php include('tpl-inc/header.php'); ?>

    <main id="main">

        <div class="section-top">
            <div class="section-inner">
                <div class="section-wrap">
                    <div class="text-wrap">
                        <h1 class="title"><span class="t">CITY CENTRAL</span></h1>
                        <div class="text formated-output">
                            <p>With the new Elizabeth Line, Farringdon will be less than 45 minutes from a quarter of the UK’s population. Travel time to Heathrow Airport is only 32 minutes by public transport. More locally, there’s a diverse scene, with 24 bars and restaurants under five minutes’ walk from 20 Farringdon Street’s doors.</p>
                            <p>London’s North-South Cycle Superhighway runs directly past 20 Farringdon Street. And with racks for 142 bikes, handy lockers, inviting showers and an on-site bicycle engineer; everything’s on hand to safely enjoy the healthiest, greenest and often speediest of commutes.</p>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <span class="image" style="background-image:url('public/i/img01.png');"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-location-directions">
            <div class="section-inner">
                <div class="section-title">
                    <span class="station-signs">
                        <img src="public/i/station-symbol-crossrail.svg" alt="Crossrail"><br>
                        <img src="public/i/station-symbol-line-circle.svg" alt="Circle">
                        <img src="public/i/station-symbol-line-metropolitan.svg" alt="Metropolitan">
                        <img src="public/i/station-symbol-line-hammersmith.svg" alt="Hammersmith & City">
                        <img src="public/i/station-symbol-overground.svg" alt="Overground">
                        <img src="public/i/station-symbol-rail.svg" alt="National Rail">
                        <!--<img src="public/i/station-symbol-airport.svg" alt="Airport">
                        <img src="public/i/station-symbol-rail.svg" alt="National Rail">
                        <img src="public/i/station-symbol-crossrail.svg" alt="Crossrail">
                        <img src="public/i/station-symbol-overground.svg" alt="Overground">
                        <img src="public/i/station-symbol-line-bakerloo.svg" alt="Bakerloo">
                        <img src="public/i/station-symbol-line-central.svg" alt="Central">
                        <img src="public/i/station-symbol-line-circle.svg" alt="Circle">
                        <img src="public/i/station-symbol-line-district.svg" alt="District">
                        <img src="public/i/station-symbol-line-hammersmith.svg" alt="Hammersmith & City">
                        <img src="public/i/station-symbol-line-jubilee.svg" alt="Jubilee">
                        <img src="public/i/station-symbol-line-metropolitan.svg" alt="Metropolitan">
                        <img src="public/i/station-symbol-line-northern.svg" alt="Northern">
                        <img src="public/i/station-symbol-line-piccadilly.svg" alt="Piccadilly">
                        <img src="public/i/station-symbol-line-victoria.svg" alt="Victoria">
                        <img src="public/i/station-symbol-line-waterloo.svg" alt="Waterloo & City">-->
                    </span>
                    <h2 class="title">FARRINGDON <br>STATION</h2>
                </div>
                <div class="directions">
                    <div class="direction">
                        <h3 class="direction-name"><span class="l">Direction</span><span class="t">LUTON AIRPORT</span></h3>
                        <ul class="stations">
                            <li class="station">
                                <span class="station-signs">
                                    <img src="public/i/station-symbol-line-northern.svg" alt="Northern">
                                    <img src="public/i/station-symbol-line-piccadilly.svg" alt="Piccadilly">
                                    <img src="public/i/station-symbol-line-victoria.svg" alt="Victoria">
                                    <img src="public/i/station-symbol-line-circle.svg" alt="Circle">
                                    <img src="public/i/station-symbol-line-metropolitan.svg" alt="Metropolitan">
                                    <img src="public/i/station-symbol-line-hammersmith.svg" alt="Hammersmith & City">
                                    <img src="public/i/station-symbol-rail.svg" alt="National Rail">
                                </span>
                                <span class="name-wrap">
                                    <h4 class="station-name">KING’S CROSS /<br>ST. PANCRAS</h4>
                                    <span class="time">4 mins</span>
                                </span>
                            </li>
                            <li class="station">
                                <span class="station-signs">
                                    <img src="public/i/station-symbol-crossrail.svg" alt="Crossrail">
                                    <img src="public/i/station-symbol-crossrail.svg" alt="Crossrail">
                                    <img src="public/i/station-symbol-crossrail.svg" alt="Crossrail">
                                    <img src="public/i/station-symbol-rail.svg" alt="National Rail">
                                </span>
                                <span class="name-wrap">
                                    <h4 class="station-name">EUSTON</h4>
                                    <span class="time">12 min</span>
                                </span>
                            </li>
                            <li class="station">
                                <span class="station-signs">
                                    <img src="public/i/station-symbol-airport.svg" alt="Airport">
                                    <img src="public/i/station-symbol-rail.svg" alt="National Rail">
                                </span>
                                <span class="name-wrap">
                                    <h4 class="station-name">LUTON <br>AIRPORT</h4>
                                    <span class="time">41 min</span>
                                </span>
                            </li>
                        </ul>
                    </div><?php //direction ?>
                    <div class="direction">
                        <h3 class="direction-name"><span class="l">Direction</span><span class="t">WOOLWICH</span></h3>
                        <ul class="stations">
                            <li class="station">
                                <span class="station-signs">
                                    <img src="public/i/station-symbol-crossrail.svg" alt="Crossrail"><br>
                                    <img src="public/i/station-symbol-line-central.svg" alt="Central">
                                    <img src="public/i/station-symbol-line-circle.svg" alt="Circle">
                                    <img src="public/i/station-symbol-line-metropolitan.svg" alt="Metropolitan">
                                    <img src="public/i/station-symbol-line-hammersmith.svg" alt="Hammersmith & City">
                                    <img src="public/i/station-symbol-overground.svg" alt="Overground">
                                    <img src="public/i/station-symbol-rail.svg" alt="National Rail">
                                </span>
                                <span class="name-wrap">
                                    <h4 class="station-name">LIVERPOOL <br>STREET</h4>
                                    <span class="time">2 mins</span>
                                </span>
                            </li>
                            <li class="station">
                                <span class="station-signs">
                                    <img src="public/i/station-symbol-crossrail.svg" alt="Crossrail"><br>
                                    <img src="public/i/station-symbol-line-district.svg" alt="District">
                                    <img src="public/i/station-symbol-line-hammersmith.svg" alt="Hammersmith & City">
                                    <img src="public/i/station-symbol-overground.svg" alt="Overground">
                                </span>
                                <span class="name-wrap">
                                    <h4 class="station-name">WHITECHAPEL</h4>
                                    <span class="time">5 min</span>
                                </span>
                            </li>
                            <li class="station">
                                <span class="station-signs">
                                    <img src="public/i/station-symbol-crossrail.svg" alt="Crossrail"><br>
                                    <img src="public/i/station-symbol-line-jubilee.svg" alt="Jubilee">
                                    <img src="public/i/station-symbol-line-waterloo.svg" alt="Waterloo & City">
                                </span>
                                <span class="name-wrap">
                                    <h4 class="station-name">CANARY <br>WHARF</h4>
                                    <span class="time">8 min</span>
                                </span>
                            </li>
                            <li class="station">
                                <span class="station-signs">
                                    <img src="public/i/station-symbol-crossrail.svg" alt="Crossrail"><br><img src="public/i/station-symbol-line-waterloo.svg" alt="Waterloo & City"><img src="public/i/station-symbol-rail.svg" alt="National Rail">
                                </span>
                                <span class="name-wrap">
                                    <h4 class="station-name">WOOLWICH</h4>
                                    <span class="time">17 min</span>
                                </span>
                            </li>
                        </ul>
                    </div><?php //direction ?>
                    <div class="direction">
                        <h3 class="direction-name"><span class="l">Direction</span><span class="t">BRIGHTON</span></h3>
                        <ul class="stations">
                            <li class="station">
                                <span class="station-signs">
                                    <img src="public/i/station-symbol-line-district.svg" alt="District">
                                    <img src="public/i/station-symbol-line-circle.svg" alt="Circle">
                                    <img src="public/i/station-symbol-rail.svg" alt="National Rail">
                                </span>
                                <span class="name-wrap">
                                    <h4 class="station-name">BLACKFRIARS</h4>
                                    <span class="time">6 mins</span>
                                </span>
                            </li>
                            <li class="station">
                                <span class="station-signs">
                                    <img src="public/i/station-symbol-airport.svg" alt="Airport">
                                    <img src="public/i/station-symbol-rail.svg" alt="National Rail">
                                </span>
                                <span class="name-wrap">
                                    <h4 class="station-name">GATWICK <br>AIRPORT</h4>
                                    <span class="time">55 min</span>
                                </span>
                            </li>
                            <li class="station">
                                <span class="station-signs">
                                    <img src="public/i/station-symbol-rail.svg" alt="National Rail">
                                </span>
                                <span class="name-wrap">
                                    <h4 class="station-name">BRIGHTON</h4>
                                    <span class="time">89 min</span>
                                </span>
                            </li>
                        </ul>
                    </div><?php //direction ?>
                    <div class="direction">
                        <h3 class="direction-name"><span class="l">Direction</span><span class="t">HEATHROW</span></h3>
                        <ul class="stations">
                            <li class="station">
                                <span class="station-signs">
                                    <img src="public/i/station-symbol-crossrail.svg" alt="Crossrail"><br>
                                    <img src="public/i/station-symbol-line-central.svg" alt="Central">
                                    <img src="public/i/station-symbol-line-northern.svg" alt="Northern">
                                </span>
                                <span class="name-wrap">
                                    <h4 class="station-name">TOTTENHAN <br>COURT ROAD</h4>
                                    <span class="time">2 mins</span>
                                </span>
                            </li>
                            <li class="station">
                                <span class="station-signs">
                                    <img src="public/i/station-symbol-crossrail.svg" alt="Crossrail"><br>
                                    <img src="public/i/station-symbol-line-jubilee.svg" alt="Jubilee">
                                    <img src="public/i/station-symbol-line-central.svg" alt="Central">
                                </span>
                                <span class="name-wrap">
                                    <h4 class="station-name">BOND <br>STREET</h4>
                                    <span class="time">4 min</span>
                                </span>
                            </li>
                            <li class="station">
                                <span class="station-signs">
                                    <img src="public/i/station-symbol-crossrail.svg" alt="Crossrail"><br>
                                    <img src="public/i/station-symbol-line-circle.svg" alt="Circle">
                                    <img src="public/i/station-symbol-line-district.svg" alt="District">
                                    <img src="public/i/station-symbol-line-hammersmith.svg" alt="Hammersmith & City">
                                    <img src="public/i/station-symbol-line-metropolitan.svg" alt="Metropolitan">
                                    <img src="public/i/station-symbol-rail.svg" alt="National Rail">
                                </span>
                                <span class="name-wrap">
                                    <h4 class="station-name">PADDINGTON</h4>
                                    <span class="time">8 min</span>
                                </span>
                            </li>
                            <li class="station">
                                <span class="station-signs">
                                    <img src="public/i/station-symbol-crossrail.svg" alt="Crossrail"><br>
                                    <img src="public/i/station-symbol-line-piccadilly.svg" alt="Piccadilly">
                                </span>
                                <span class="name-wrap">
                                    <h4 class="station-name">HEATHROW <br>AIRPORT</h4>
                                    <span class="time">32 min</span>
                                </span>
                            </li>
                        </ul>
                    </div><?php //direction ?>
                </div>
            </div>
        </div>

        <div class="section-location-discover">
            <div class="map-holder">
                <!--<div class="map-image-placeholder" style="background-image:url('public/i/img-map.png');"></div>-->
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d85168.2217370349!2d17.113088!3d48.1583104!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2ssk!4v1527335998573" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="section-inner">
                <h2 class="el-section-title">DISCOVER <br>THE NEIGHBOURHOOD</h2>
                <div class="row">
                    <div class="map-wrap">
                        <div class="map-placeholder"></div>
                    </div>
                    <div id="accordion" class="data-wrap">
                        <h3 class="acc-title">AMENITIES</h3>
                        <div>
                            <h4 class="acc-subtitle" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">BARS &amp; RESTAURANTS</h4>
                            <div id="collapse1" class="acc-content collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <ol>
                                    <li><a href="/">ALCHEMY COFFEE</a></li>
                                    <li><a href="/">BEAS CAKE BOUTIQUE</a></li>
                                    <li><a href="/">BENIHANA</a></li>
                                    <li><a href="/">BURGER &amp; LOBSTER</a></li>
                                    <li><a href="/">CEENA</a></li>
                                    <li><a href="/">CORNEY &amp; BARROW FLEET PLACE</a></li>
                                    <li><a href="/">DADO 54</a></li>
                                    <li><a href="/">FOXLOW</a></li>
                                </ol>
                            </div>
                            <h4 class="acc-subtitle collapsed" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">RETAIL</h4>
                            <div id="collapse2" class="acc-content collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <ol>
                                    <li><a href="/">ALCHEMY COFFEE</a></li>
                                    <li><a href="/">BEAS CAKE BOUTIQUE</a></li>
                                    <li><a href="/">BENIHANA</a></li>
                                    <li><a href="/">BURGER &amp; LOBSTER</a></li>
                                    <li><a href="/">CEENA</a></li>
                                    <li><a href="/">CORNEY &amp; BARROW FLEET PLACE</a></li>
                                    <li><a href="/">DADO 54</a></li>
                                    <li><a href="/">FOXLOW</a></li>
                                </ol>
                            </div>
                        </div>
                        <h3 class="acc-title">OCCUPIERS</h3>
                        <div>
                            <h4 class="acc-subtitle collapsed" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">CORPORATE</h4>
                            <div id="collapse3" class="acc-content collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                <ol>
                                    <li><a href="/">ALCHEMY COFFEE</a></li>
                                    <li><a href="/">BEAS CAKE BOUTIQUE</a></li>
                                    <li><a href="/">BENIHANA</a></li>
                                    <li><a href="/">BURGER &amp; LOBSTER</a></li>
                                    <li><a href="/">CEENA</a></li>
                                    <li><a href="/">CORNEY &amp; BARROW FLEET PLACE</a></li>
                                    <li><a href="/">DADO 54</a></li>
                                    <li><a href="/">FOXLOW</a></li>
                                </ol>
                            </div>
                            <h4 class="acc-subtitle collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">FINNANCIAL</h4>
                            <div id="collapse4" class="acc-content collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                <ol>
                                    <li><a href="/">ALCHEMY COFFEE</a></li>
                                    <li><a href="/">BEAS CAKE BOUTIQUE</a></li>
                                    <li><a href="/">BENIHANA</a></li>
                                    <li><a href="/">BURGER &amp; LOBSTER</a></li>
                                    <li><a href="/">CEENA</a></li>
                                    <li><a href="/">CORNEY &amp; BARROW FLEET PLACE</a></li>
                                    <li><a href="/">DADO 54</a></li>
                                    <li><a href="/">FOXLOW</a></li>
                                </ol>
                            </div>
                        </div>
                    </div><?php //#accordion ?>
                </div>
            </div>
        </div>

        <div class="section-slider section-location-slider">
            <div class="section-inner">
                <h2 class="el-section-title">GET INSPIRED IN FARRINGDON</h2>
            </div>
            <div class="section-bg">
                <div class="section-inner">
                    <div class="slider">
                        <div class="slider-item"><a href="/"><span class="image" style="background-image:url('public/i/img-slide01.jpg');"></span><span class="description">BREDDOS</span></a></div>
                        <div class="slider-item"><a href="/"><span class="image" style="background-image:url('public/i/img-slide01.jpg');"></span><span class="description">BREDDOS</span></a></div>
                        <div class="slider-item"><a href="/"><span class="image" style="background-image:url('public/i/img-slide01.jpg');"></span><span class="description">BREDDOS</span></a></div>
                        <div class="slider-item"><a href="/"><span class="image" style="background-image:url('public/i/img-slide01.jpg');"></span><span class="description">BREDDOS</span></a></div>
                        <div class="slider-item"><a href="/"><span class="image" style="background-image:url('public/i/img-slide01.jpg');"></span><span class="description">BREDDOS</span></a></div>
                        <div class="slider-item"><a href="/"><span class="image" style="background-image:url('public/i/img-slide01.jpg');"></span><span class="description">BREDDOS</span></a></div>
                        <div class="slider-item"><a href="/"><span class="image" style="background-image:url('public/i/img-slide01.jpg');"></span><span class="description">BREDDOS</span></a></div>
                        <div class="slider-item"><a href="/"><span class="image" style="background-image:url('public/i/img-slide01.jpg');"></span><span class="description">BREDDOS</span></a></div>
                        <div class="slider-item"><a href="/"><span class="image" style="background-image:url('public/i/img-slide01.jpg');"></span><span class="description">BREDDOS</span></a></div>
                    </div>
                </div>
            </div>
        </div>

        <?php include('tpl-inc/inc-section-contact.php'); ?>

    </main>

    <?php include('tpl-inc/footer.php'); ?>

</div>

<?php include('tpl-inc/foot.php'); ?>