<?php
$pageTitle = 'HB Reavis - 20Farringdon';
$pageClass = 'building';
$pageName = 'building';
?>
<?php include('tpl-inc/head.php'); ?>

<div id="page" class="<?php print $pageClass; ?>">

    <?php include('tpl-inc/cookies.inc.php'); ?>
    <?php include('tpl-inc/header.php'); ?>

    <main id="main">

        <div class="section-top">
            <div class="section-inner">
                <div class="section-wrap">
                    <div class="text-wrap">
                        <h1 class="title"><span class="t">THE BUILDING</span></h1>
                        <div class="text formated-output">
                            <p>With a double height ceiling, facial recognition security and quality; the lobby feels like a bustling business lounge. Complete with a striking bespoke feature light that flows on from reception, and up 20 Farringdon Street’s feature stairs.</p>
                            <p>The bespoke lighting leads through reception and follows a path up the double width polished concrete staircase. A guide through the building, the feature also provides an illuminating daily exercise to strengthen body and soul.</p>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <div class="slider">
                            <div class="slide">
                                <span class="image" style="background-image:url('public/i/building-01.png');"></span>
                            </div>
                            <div class="slide">
                                <span class="image" style="background-image:url('public/i/img01.png');"></span>
                            </div>
                            <div class="slide">
                                <span class="image" style="background-image:url('public/i/img-slide01.jpg');"></span>
                            </div>
                            <div class="slide">
                                <span class="image" style="background-image:url('public/i/img-news02.jpg');"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-building-specifications">
            <div class="section-inner">



                <div class="text-holder text-holder--1">
                    <h2 class="el-section-title">SPECIFICATIONS</h2>
                    <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat. Facere possimus, omnis voluptas assumenda est.</p>
                </div>


                <div class="diamonds-grid">
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-01.svg" alt=""></span>
                                    <span class="t">CAT B FIT OUT <br>OPTION</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <span class="grid-item item-space"><span href="/" class="item"><span class="item-inner"></span></span></span>
                    <span class="grid-item item-space"><span href="/" class="item"><span class="item-inner"></span></span></span>
                    <span class="grid-item item-space"><span href="/" class="item"><span class="item-inner"></span></span></span>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-02.svg" alt=""></span>
                                    <span class="t">142 BICYCLE SPACES <br>AND LOCKERS</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <span class="grid-item item-space"><span href="/" class="item"><span class="item-inner"></span></span></span>
                    <span class="grid-item item-space"><span href="/" class="item"><span class="item-inner"></span></span></span>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-03.svg" alt=""></span>
                                    <span class="t">NEW GRADE A <br>DEVELOPMENT</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-04.svg" alt=""></span>
                                    <span class="t">4 PASSENGER <br>LIFTS</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-05.svg" alt=""></span>
                                    <span class="t">DOUBLE-WIDTH <br>FEATURE STAIRCASE</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-06.svg" alt=""></span>
                                    <span class="t">4 PIPE FAN COIL <br>AIR-CONDITIONING</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-07.svg" alt=""></span>
                                    <span class="t">14 <br>SHOWERS</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-08.svg" alt=""></span>
                                    <span class="t">FULLY ACCESSIBLE <br>RAISED FLOORS</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-09.svg" alt=""></span>
                                    <span class="t">FLOOR TO CEILING <br>HEIGHT 3.2M</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-10.svg" alt=""></span>
                                    <span class="t">1:8 OCCUPATION <br>DENSITY</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-11.svg" alt=""></span>
                                    <span class="t">TAXI <br>DROP-OFF POINT</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-12.svg" alt=""></span>
                                    <span class="t">6 EXTERNAL TERRACES</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-13.svg" alt=""></span>
                                    <span class="t">1GB FIBRE CONNECTED <br>BUILDING</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <span class="grid-item item-space"><span href="/" class="item"><span class="item-inner"></span></span></span>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-14.svg" alt=""></span>
                                    <span class="t">BREAM RATING <br>‘EXELLENT’</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-15.svg" alt=""></span>
                                    <span class="t">FACIAL RECOGNITION</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <span class="grid-item item-space"><span href="/" class="item"><span class="item-inner"></span></span></span>
                    <span class="grid-item item-space"><span href="/" class="item"><span class="item-inner"></span></span></span>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-16.svg" alt=""></span>
                                    <span class="t">LED <br>LIGHTING</span>
                                </span>
                            </span>
                        </a>
                    </div>
                    <div class="grid-item">
                        <a href="/" class="item">
                            <span class="item-inner">
                                <span class="item-inner-wrap">
                                    <span class="i"><img src="public/i/bs-icon-17.svg" alt=""></span>
                                    <span class="t">TOWEL <br>SERVICE</span>
                                </span>
                            </span>
                        </a>
                    </div>
                </div><?php //diamonds-grid ?>




                <div class="text-holder text-holder--2">
                    <h2 class="el-section-title">SCHEDULE OF AREAS</h2>
                    <p>Column-free floors are a blank canvas for open plans, enclosed snugs, grand meeting rooms and collaborative workspaces. The 9th, 10th and 11th floor terraces make for rich break out areas and meeting venues. They inject sky, air and inspiration to the working culture – and a modern mark of status.</p>
                </div>




            </div>
        </div>

        <div class="section-text-left section-building-areas">
            <div class="tabs-wrap">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><span>FLOOR AREAS</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><span>FLOOR PLANS</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="messages-tab" data-toggle="tab" href="#messages" role="tab" aria-controls="messages" aria-selected="false"><span>SPACE PLANS</span></a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-md-7 col-lg-8">


                            <figure class="floor-plan">
                                <h6 class="floor-plan__name">10th Floor</h6>
                                <p class="floor-plan__description">6,382 sq ft / 593 sq m</p>
                                <div class="floor-plan__image">
                                    <img src="public/i/floor-plan.png" alt="">
                                </div>
                                <div class="floor-plan__control floor-plan__control--download"></div>
                                <div class="floor-plan__control floor-plan__control--expand js-popup-trigger-1"></div>
                                <div class="floor-plan__control floor-plan__control--previous"></div>
                                <div class="floor-plan__control floor-plan__control--next"></div>
                                <div class="floor-plan__compass"></div>
                            </figure>

                            <div class="popup-floor-plan js-popup-1">
                                <div class="popup-floor-plan__overlay js-popup-trigger-1"></div>
                                <div class="popup-floor-plan__plate">
                                    <div class="popup-floor-plan__header">
                                        <h6 class="popup-floor-plan__name">10th Floor</h6>
                                        <p class="popup-floor-plan__description">6,382 sq ft / 593 sq m</p>
                                        <button class="popup-floor-plan__close js-popup-trigger-1"></button>
                                    </div>
                                    <div class="popup-floor-plan__content">
                                        <div class="popup-floor-plan__image">
                                            <img src="public/i/floor-plan.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>
                        <div class="col-md-5 col-lg-4">
                            <div class="params-table">
                                <div class="t-row t-head">
                                    <div class="t-cell">FLOOR</div>
                                    <div class="t-cell">SQ FT</div>
                                    <div class="t-cell">SQ M</div>
                                </div>
                                <div class="t-multi-row selectable selected">
                                    <div class="t-row">
                                        <div class="t-cell">11TH</div>
                                        <div class="t-cell">5,557</div>
                                        <div class="t-cell">516</div>
                                    </div>
                                    <div class="t-row">
                                        <div class="t-cell">TERRACE AREA</div>
                                        <div class="t-cell">721</div>
                                        <div class="t-cell">67</div>
                                    </div>
                                </div>
                                <div class="t-multi-row selectable">
                                    <div class="t-row">
                                        <div class="t-cell">10TH</div>
                                        <div class="t-cell">6,382</div>
                                        <div class="t-cell">593</div>
                                    </div>
                                    <div class="t-row">
                                        <div class="t-cell">TERRACE AREA</div>
                                        <div class="t-cell">624</div>
                                        <div class="t-cell">58</div>
                                    </div>
                                </div>
                                <div class="t-multi-row selectable">
                                    <div class="t-row">
                                        <div class="t-cell">9TH</div>
                                        <div class="t-cell">7,110</div>
                                        <div class="t-cell">661</div>
                                    </div>
                                    <div class="t-row">
                                        <div class="t-cell">TERRACE AREA</div>
                                        <div class="t-cell">1,173</div>
                                        <div class="t-cell">108</div>
                                    </div>
                                </div>
                                <div class="t-row selectable">
                                    <div class="t-cell">8TH</div>
                                    <div class="t-cell">8,622</div>
                                    <div class="t-cell">801</div>
                                </div>
                                <div class="t-row selectable">
                                    <div class="t-cell">7TH</div>
                                    <div class="t-cell">8,622</div>
                                    <div class="t-cell">801</div>
                                </div>
                                <div class="t-row selectable">
                                    <div class="t-cell">6TH</div>
                                    <div class="t-cell">8,622</div>
                                    <div class="t-cell">801</div>
                                </div>
                                <div class="t-row selectable">
                                    <div class="t-cell">5TH</div>
                                    <div class="t-cell">8,622</div>
                                    <div class="t-cell">801</div>
                                </div>
                                <div class="t-row selectable">
                                    <div class="t-cell">4TH</div>
                                    <div class="t-cell">8,622</div>
                                    <div class="t-cell">801</div>
                                </div>
                                <div class="t-row selectable">
                                    <div class="t-cell">3RD</div>
                                    <div class="t-cell">8,622</div>
                                    <div class="t-cell">801</div>
                                </div>
                                <div class="t-row selectable">
                                    <div class="t-cell">2ND</div>
                                    <div class="t-cell">8,589</div>
                                    <div class="t-cell">798</div>
                                </div>
                                <div class="t-row selectable">
                                    <div class="t-cell">1ST</div>
                                    <div class="t-cell">5,742</div>
                                    <div class="t-cell">533</div>
                                </div>
                                <div class="t-row">
                                    <div class="t-cell">Ground</div>
                                    <div class="t-cell">-</div>
                                    <div class="t-cell">-</div>
                                </div>
                                <div class="t-row t-foot">
                                    <div class="t-cell">TOTAL</div>
                                    <div class="t-cell">85,112</div>
                                    <div class="t-cell">7,907</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="row">
                        <div class="col-6">
                            yyyyyyy
                        </div>
                        <div class="col-6">
                            yyyyyyy
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="messages" role="tabpanel" aria-labelledby="messages-tab">
                    <div class="row">
                        <div class="col-6">
                            zzzzzzz
                        </div>
                        <div class="col-6">
                            zzzzzzz
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include('tpl-inc/inc-section-contact.php'); ?>

    </main>

    <?php include('tpl-inc/footer.php'); ?>

</div>

<?php include('tpl-inc/foot.php'); ?>